package com.appcdn.fausto.appcdn;

import android.content.Context;
import android.util.Log;

import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSink;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;

import java.io.File;

/**
 * Created by Utente on 04/07/2017.
 */

public class CacheDataSourceFactoryHandler {

    String videoPath = "media_cache";

    public void changeVideoPath(String videoPath){
        this.videoPath = videoPath;
    }

    //Questo listener provvede a monitorare pacchetto per pacchetto il bitrate
    public BandwidthMeter.EventListener bandwidthMeterListener = new BandwidthMeter.EventListener() {
        @Override
        public void onBandwidthSample(int elapsedMs, long bytes, long bitrate) {
            Log.d("playerLoggerBandwidth", "onBandwidthSample() triggered con elapseMs: " + elapsedMs + " con bytes: " + bytes + " con bitrate: " + bitrate);
            //Qui va l'algoritmo di bandwidth meter
        }
    };

    //Questo listener provvede a monitorare se un pacchetto viene letto dalla cache oppure no
    public CacheDataSource.EventListener cacheDataSourceEventListener = new CacheDataSource.EventListener() {
        @Override
        public void onCachedBytesRead(long cacheSizeBytes, long cachedBytesRead) {
            Log.d("cacheDataSourceLogger", "onCachedBytesRead triggered con cacheSizeBytes: " + cacheSizeBytes + " e cachedBytesRead: " + cachedBytesRead);
        }
    };

    private final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter(null, bandwidthMeterListener);
    private Context context;

    public CacheDataSourceFactoryHandler(Context context){
        this.context = context;
    }

    DataSource.Factory buildDataSourceFactory(final boolean cache) {

        if (!cache) {
            return new DefaultDataSourceFactory(context, BANDWIDTH_METER,
                    buildHttpDataSourceFactory(BANDWIDTH_METER));
        }else{

            return new DataSource.Factory() {
                @Override
                public DataSource createDataSource() {
                    File cacheDir = new File(context.getCacheDir(), videoPath);
                    LeastRecentlyUsedCacheEvictor evictor = new LeastRecentlyUsedCacheEvictor(100 * 1024 * 1024);
                    SimpleCache simpleCache = new SimpleCache(cacheDir, evictor);
                    //simpleCache.addListener(videoPath, cacheListener);
                    SharedCacheDir sharedCacheDir = new SharedCacheDir();
                    sharedCacheDir.setCacheDir(cacheDir);
                    sharedCacheDir.setSimpleCache(simpleCache);

                    Log.d("cacheLogger", "Ecco la cacheDir: " + cacheDir.toString());
                    Log.d("cacheLogger","Ecco la lista delle chiavi per la cache attuale: " + simpleCache.getKeys().toString());


                    return new CacheDataSource(simpleCache, buildCachedHttpDataSourceFactory(BANDWIDTH_METER).createDataSource(),
                            new FileDataSource(), new CacheDataSink(simpleCache, 10 * 1024 * 1024),
                            CacheDataSource.FLAG_BLOCK_ON_CACHE | CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR, cacheDataSourceEventListener);
                }

            };
        }
    }
    private DefaultDataSource.Factory buildCachedHttpDataSourceFactory(DefaultBandwidthMeter bandwidthMeter) {
        return new DefaultDataSourceFactory(context, bandwidthMeter, buildHttpDataSourceFactory(bandwidthMeter));
    }

    private DefaultHttpDataSource.Factory buildHttpDataSourceFactory(DefaultBandwidthMeter bandwidthMeter) {
        return new DefaultHttpDataSourceFactory("Exoplayer", bandwidthMeter);
    }

    public static class SharedCacheDir{
        private static File cacheDir;
        private static SimpleCache simpleCache;

        public static void setCacheDir(File cacheDir){
            cacheDir = cacheDir;
        }

        public static File getCacheDir(){
            return cacheDir;
        }

        public static void setSimpleCache(SimpleCache simpleCache){
            simpleCache = simpleCache;
        }

        public static SimpleCache getSimpleCache(){
            return simpleCache;
        }
    }
}

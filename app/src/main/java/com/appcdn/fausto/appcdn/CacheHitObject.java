package com.appcdn.fausto.appcdn;

import android.util.Log;

import java.text.DecimalFormat;

/**
 * Created by Utente on 29/11/2017.
 */

public class CacheHitObject {

    private String nomeDevice;
    //private ProgressBar progressBar;
    private int progress = 0;
    private int maxProgress = 0;
    private int length;
    private double precisePercentage = 0.0;

    public CacheHitObject(String nomeDevice, int length){
        this.nomeDevice = nomeDevice;
        //this.progressBar = progressBar;
        this.length = length;
    }

    public String getNomeDevice(){
        return this.nomeDevice;
    }

    public void setNomeDevice(String nomeDevice){
        this.nomeDevice = nomeDevice;
    }

    /*
    public ProgressBar getProgressBar(){
        return this.progressBar;
    }


    public void setProgressBar(ProgressBar progressBar){
        this.progressBar = progressBar;
    }

*/
    public int getProgress(){
        return  this.progress;
    }

    public void setProgress(int progress){
        this.progress = progress;
    }

    public int getThisLength(){
        return this.length;
    }

    public void setThisLength(int length){
        this.length = length;
    }

    public void addProgress(int value){
        this.progress = this.progress + value;
    }

    public int getMaxProgress() {
        return maxProgress;
    }

    public void setMaxProgress(int maxProgress) {
        this.maxProgress = maxProgress;
    }

    public void setPrecisePercentage(double result){
        this.precisePercentage = result;
    }

    public double getPrecisePercentage(){
        return this.precisePercentage;
    }

    public int getPercentuale(){
        int percentuale = 0;
        if(progress != 0 && maxProgress != 0){
            double result = (double) progress/maxProgress;
            double percentualeDouble = result*100;
            Double resultDouble = new Double(percentualeDouble);
            percentuale = resultDouble.intValue();
        }
        return percentuale;
    }

    public double changeDecimalFormat(double precisePercentage){
        double newFormat = 0.0;
        try {
            DecimalFormat decimalFormat = new DecimalFormat("##.###");
            String newFormatString = decimalFormat.format(precisePercentage);
            newFormat = Double.parseDouble(newFormatString);
        }
        catch (NumberFormatException e){
            Log.d("connectionLogger", "Errore strano con il numberformatexception");
            e.printStackTrace();
        }
        return newFormat;
    }

    public double getPercentage(){
        double percentage = 0.0;
        if(progress != 0 && maxProgress != 0){
            double result = (double) progress/maxProgress;
            percentage = changeDecimalFormat(result);
        }
        return percentage;
    }

}

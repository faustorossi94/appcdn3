package com.appcdn.fausto.appcdn;

import android.util.Log;

import java.io.File;

/**
 * Created by Utente on 14/10/2017.
 */

public class CreateDirectory {

    //Default constructor
    public CreateDirectory(){

    }

    //Metodo per creare una directoy se non esiste
    private void createDirectory(File directory){
        if (!directory.exists()) {
            Log.d("mpdDirectory", "creating directory: " + directory.getName());
            boolean result = false;

            try{
                directory.mkdir();
                result = true;
            }
            catch(SecurityException se){
                //handle it
            }
            if(result) {
                Log.d("mpdDirectory", "DIR created");
            }
            else{
                Log.d("mpdDirectory", "Directory already created");
            }
        }
    }

    public void directoryCreator(File directory){
        createDirectory(directory);
    }
}

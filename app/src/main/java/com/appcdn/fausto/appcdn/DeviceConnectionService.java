package com.appcdn.fausto.appcdn;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.net.wifi.p2p.WifiP2pManager.ConnectionInfoListener;
import android.net.wifi.p2p.WifiP2pManager.GroupInfoListener;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.appcdn.fausto.appcdn.MainActivity.SharedClientActivated;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource.EventListener;

import org.json.JSONObject;

import static android.util.Log.d;

/**
 * Created by Utente on 06/07/2017.
 */

public class DeviceConnectionService extends Service {

    IBinder iBinder = new Binder();

    //Varibili per la gestione del WifiDirect
    WifiP2pManager mManager;
    Channel mChannel;
    WiFiDirectBroadcastReceiver mReceiver;

    int mainAction;
    String sourceIntent;

    String chunkKey;
    String videoPath;
    //long realDurationMillis;

    ThreadClient threadClient = null;
    ThreadPoolServer threadPoolServer = null;

    WifiP2pDevice host;

    WifiP2pManager.ConnectionInfoListener connectionInfoListener;
    WifiP2pManager.ConnectionInfoListener disconnectionInfoListener;

    JSONObject responseData;

    CacheDataSource.EventListener cacheDataSourceEventListener;

    MainActivity.SharedClientActivated sharedClientActivated;
    MainActivity.SharedServerActivated sharedServerActivated;
    MainActivity.SharedMainActivity sharedMainActivity;

    MainActivity mainActivity;

    ThreadActiveInstance threadActiveInstance;

    EvaluationStream evaluationStream;
    SharedEvaluationStream sharedEvaluationStream;

    private void connect(WifiP2pDevice host){

        d("debugTime","Siamo nel connect");

        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = host.deviceAddress;
        config.wps.setup = WpsInfo.PBC;
        config.groupOwnerIntent = 0;

        d("debugTime","Il config è settato con l'host e ha valori " + config.toString());

        mManager.connect(mChannel, config, new ActionListener() {
            @Override
            public void onSuccess() {
                //Aggancio avvenuto
                d("debugTime", "Aggancio avvenuto al dispositivo");
            }

            @Override
            public void onFailure(int reason) {
                //Aggancio non avvenuto
                d("debugTime", "Errore nell'aggancio");
            }
        });
    }

    public void createGroup(){
        mManager.createGroup(mChannel, new WifiP2pManager.ActionListener(){
            public void onSuccess(){
                d("debugTime", "Gruppo creato con successo");
            }
            public void onFailure(int reasonCode){
                Log.e("debugTime", "Errore nella creazione del gruppo con errore " + reasonCode);
            }
        });
    }

    public void removeGroup(){
        if (mManager != null && mChannel != null) {
            mManager.requestGroupInfo(mChannel, new GroupInfoListener() {
                @Override
                public void onGroupInfoAvailable(WifiP2pGroup group) {
                    if (group != null && mManager != null && mChannel != null) {
                        mManager.removeGroup(mChannel, new ActionListener() {

                            @Override
                            public void onSuccess() {
                                d("debugTime", "removeGroup onSuccess -");
                                try{
                                    threadClient.closeConnection();
                                }
                                catch (NullPointerException e){
                                    Log.d("debugTime", "Il thread client non è attivo");
                                }
                            }

                            @Override
                            public void onFailure(int reason) {
                                d("debugTime", "removeGroup onFailure -" + reason);
                            }
                        });
                    }
                }
            });
        }
    }

    public void wakeUpThreadClient(String chunkKey, String videoPath /*long realDurationMillis*/){
        try {
            threadClient.wakeUp(chunkKey, videoPath /*realDurationMillis*/);
        }
        catch(NullPointerException e){
            Log.d("connectionLogger", "Thread Client non avviato");
        }
    }

    //Inizializzazione del servizio
    public void onCreate(){
        super.onCreate();

        mManager = MainActivity.SharedWiFiManager.getManager();
        mChannel = MainActivity.SharedWiFiManager.getChannel();
        mReceiver = MainActivity.SharedWiFiManager.getReceiver();

        //realm = Realm.getDefaultInstance();

        threadActiveInstance = new ThreadActiveInstance();

        inizializzaCacheEventListener();
        inizializzazioneConnectionPlayer();
        inizializzazioneDisconnectionPlayer();

        try {
            mReceiver.setConnectionListener(connectionInfoListener);
            mReceiver.setDisconnectionListener(disconnectionInfoListener);
        }
        catch (NullPointerException e){
            Log.d("debugTime", "Errore NullPointerException successo nel mReceiver");
            e.printStackTrace();
        }

        removeGroup();

        d("debugTime","OnCreate del Service avviato");

        mainActivity = sharedMainActivity.getMainActivity();

        evaluationStream = new EvaluationStream();
        sharedEvaluationStream.setEvaluationStream(evaluationStream);

    }

    public int onStartCommand(Intent intent, int flags, int startId){
        d("debugTime","DeviceConnectionServer Avviato");

        mainAction = intent.getIntExtra("actionService",0);
        sourceIntent = intent.getStringExtra("sourceIntent");

        d("debugTime","Sono nel DeviceConnectionService e ho ricevuto come intent questi dati: mainAction = " + mainAction);

        if(sourceIntent.equals("MainActivity.class")) {
            if (mainAction == 0) {
                //Server
                Log.d("debugTime", "Sono entrato nell'if server");
                //Se il thread del client è attivo lo spengo
                removeGroup();
                createGroup();
                sharedServerActivated.setServerActivated(true);
            } else if (mainAction == 1) {
                //Client
                d("debugTime", "Sono nell'if client");
                host = intent.getParcelableExtra("host");
                d("debugTime", "Ho questo host = " + host.toString());
                removeGroup();
                connect(host);
            } else if (mainAction == 2) {
                removeGroup();
                d("debugTime", "Sto stoppando il service");
                stopSelf();
                sharedServerActivated.setServerActivated(false);
                sharedClientActivated.setClientActivated(false);
            } else {
                //Default
                d("debugTime", "Errore nell'interfaccia");
            }
        }

        if(sourceIntent.equals("VideoPlayerActivity.class")){
            chunkKey = intent.getStringExtra("chunkKey");
            videoPath = intent.getStringExtra("videoPath");
            //realDurationMillis = intent.getLongExtra("durataVideo", 0);
            wakeUpThreadClient(chunkKey, videoPath /*realDurationMillis*/);
        }
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    //Un altro servizio può collegarsi a questo
    public IBinder onBind(Intent intent) {
        return iBinder;
    }

    //Un altro servizio può scollegarsi da questo
    public boolean onUnbind(Intent intent){
        return true;
    }

    //Un altro servizio può ricollegarsi a questo
    public void onRebind(Intent intent){
        super.onRebind(intent);
    }

    //Quando il servizio viene chiuso. OnDestroy è richiamabile quando non ci sono più Bind
    public void onDestroy(){
        super.onDestroy();
        d("debugTime","Sono nell'onDestroy del service");
        removeGroup();
    }

    public void inizializzaCacheEventListener(){
        cacheDataSourceEventListener = new EventListener() {
            @Override
            public void onCachedBytesRead(long cacheSizeBytes, long cachedBytesRead) {
                Log.d("cacheLogger", "onCachedBytesRead triggered nel DeviceConnectionService con cacheSizeBytes: " + cacheSizeBytes + " e cachedBytesRead: " + cachedBytesRead);
            }
        };
    }

    public void inizializzazioneConnectionPlayer() {

        connectionInfoListener = new ConnectionInfoListener() {
            @Override
            public void onConnectionInfoAvailable(WifiP2pInfo info) {
                // InetAddress from WifiP2pInfo struct.
                final String groupOwnerAddress = info.groupOwnerAddress.getHostAddress();

                // After the group negotiation, we can determine the group owner
                // (server).
                if (info.groupFormed && info.isGroupOwner) {
                    // Do whatever tasks are specific to the group owner.
                    // One common case is creating a group owner thread and accepting
                    // incoming connections.
                    if(threadPoolServer == null) {
                        threadPoolServer = new ThreadPoolServer(getApplicationContext(), 8888);
                        new Thread(threadPoolServer).start();
                        threadActiveInstance.setInstanceThreadPoolServer(threadPoolServer);
                        makeToast("Avviato il thread server pool");

                        //Qui va inserita la transaction per il fragment nella main activity
                        mainActivity.replaceFragment();
                    }

                } else if (info.groupFormed) {
                    // The other device acts as the peer (client). In this case,
                    // you'll want to create a peer thread that connects
                    // to the group owner.
                    if(sharedClientActivated == null) {
                        sharedClientActivated = new SharedClientActivated();
                        sharedClientActivated.setClientActivated(true);
                    }

                    if(threadClient == null) {
                        threadClient = new ThreadClient(getApplicationContext(), groupOwnerAddress);
                        threadClient.start();
                        threadActiveInstance.setInstanceThreadClient(threadClient);
                        makeToast("Avviato il thread client");
                    }
                }
            }
        };
    }

    public void makeToast(String text){
        Context context = getApplicationContext();
        //CharSequence text = "Hello toast!";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    //Bisogna valutare se serve o no
    public void inizializzazioneDisconnectionPlayer() {

        disconnectionInfoListener = new ConnectionInfoListener() {
            @Override
            public void onConnectionInfoAvailable(WifiP2pInfo info) {
                // InetAddress from WifiP2pInfo struct.
                final String groupOwnerAddress = info.groupOwnerAddress.getHostAddress();

                // After the group negotiation, we can determine the group owner
                // (server).
                if (info.isGroupOwner) {
                    // Do whatever tasks are specific to the group owner.
                    // One common case is creating a group owner thread and accepting
                    // incoming connections.


                } else {
                    // The other device acts as the peer (client). In this case,
                    // you'll want to create a peer thread that connects
                    // to the group owner.
                    if(sharedClientActivated == null) {
                        sharedClientActivated = new SharedClientActivated();
                        sharedClientActivated.setClientActivated(false);
                    }
                    else{
                        sharedClientActivated.setClientActivated(false);
                    }

                    if(threadClient != null) {
                        threadClient.closeConnection();
                        makeToast("Thread client fermato");
                    }
                }
            }
        };
    }

    public static class ThreadActiveInstance{
        private static ThreadClient instanceThreadClient;
        private static ThreadPoolServer instanceThreadPoolServer;

        public static void setInstanceThreadClient(ThreadClient thread){
            ThreadActiveInstance.instanceThreadClient = thread;
        }

        public static void setInstanceThreadPoolServer(ThreadPoolServer thread){
            ThreadActiveInstance.instanceThreadPoolServer = thread;
        }

        public static ThreadClient getInstanceThreadClient(){
            return ThreadActiveInstance.instanceThreadClient;
        }

        public static ThreadPoolServer getInstanceThreadPoolServer(){
            return ThreadActiveInstance.instanceThreadPoolServer;
        }
    }

    public static class SharedEvaluationStream{
        private static EvaluationStream evaluationStream;

        public static void setEvaluationStream(EvaluationStream evaluationStream){
            SharedEvaluationStream.evaluationStream = evaluationStream;
        }

        public static EvaluationStream getEvaluationStream(){
            return SharedEvaluationStream.evaluationStream;
        }
    }
}

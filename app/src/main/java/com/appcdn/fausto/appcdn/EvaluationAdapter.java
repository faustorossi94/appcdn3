package com.appcdn.fausto.appcdn;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utente on 28/11/2017.
 */

public class EvaluationAdapter extends ArrayAdapter<CacheHitObject> {
    LayoutInflater mInflater;
    List<CacheHitObject> deviceList = new ArrayList<>();
    Context context;

    public EvaluationAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<CacheHitObject> objects) {
        super(context, resource, objects);

        Log.d("debugTime","Siamo nel costruttore del PeerListAdapter");

        mInflater = LayoutInflater.from(context);
        deviceList = objects;
        this.context = context;
    }

    public void setDeviceList(List<CacheHitObject> deviceList){
        this.deviceList = deviceList;
    }

    @Override
    public CacheHitObject getItem(int position) {
        return getDevice(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
/*
    @Override
    public void notifyDataSetChanged() {
        // TODO Auto-generated method stub
        super.notifyDataSetChanged();
    }
*/
    @Override
    public int getCount(){
        return deviceList.size();
    }

    public CacheHitObject getDevice(int position){
        return deviceList.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        Log.d("debugTime","Siamo nel getView del EvaluationAdapter");
        // TODO Auto-generated method stub

        final CacheHitObject cacheHitObject = getItem(position);

        EvaluationAdapter.ViewHolder holder;
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.evaluation_peer_item, null);
            holder = new EvaluationAdapter.ViewHolder();
            if(getDevice(position) != null) {
                Log.d("debugTime", "Siamo nell'if dove estraiamo il nome e l'indirizzo del dispositivo");
                holder.deviceName = (TextView) convertView.findViewById(R.id.device_name);
                holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progress_bar);
                holder.cacheHitObject = cacheHitObject;
            }

            convertView.setTag(holder);
        } else {
            holder = (EvaluationAdapter.ViewHolder) convertView.getTag();

            //holder.cacheHitObject.setProgressBar(null);
            holder.cacheHitObject = cacheHitObject;
            //holder.cacheHitObject.setProgressBar(holder.progressBar);
        }

        if(getDevice(position)!= null) {
            Log.d("debugTime","Settiamo i dati del dispositivo nel setText");
            holder.deviceName.setText(deviceList.get(position).getNomeDevice());
            holder.progressBar.setProgress(cacheHitObject.getPercentuale());
            //holder.progressBar.setMax(cacheHitObject.getThisLength());
            //cacheHitObject.setProgressBar(holder.progressBar);
        }
        return convertView;
    }

    static class ViewHolder{
        TextView deviceName;
        ProgressBar progressBar;
        CacheHitObject cacheHitObject;
    }
}

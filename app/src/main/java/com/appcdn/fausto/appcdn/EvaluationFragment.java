package com.appcdn.fausto.appcdn;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utente on 27/11/2017.
 */

public class EvaluationFragment extends Fragment {
    //Questo fragment è necessario per la visualizzazione a schermo di grafici e/o percentuali per la valutazione dei risultati
    View rootView;
    EvaluationAdapter evaluationAdapter;
    ListView lv;
    private List<CacheHitObject> deviceList = new ArrayList<CacheHitObject>();
/*
    public void setDeviceList(List<CacheHitObject> deviceList){
        this.deviceList = deviceList;
    }

    public void setDeviceListAdapter(List<CacheHitObject> deviceList){
        evaluationAdapter.setDeviceList(deviceList);
    }
*/
    public void notifyChange(){
        evaluationAdapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d("debugTime","Siamo nell'onCreateView dell'EvaluationFragment");

        rootView = inflater.inflate(R.layout.evaluation_fragment, container, false);
        lv = (ListView)rootView.findViewById(R.id.evaluation_peer_list);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        Log.d("debugTime","Siamo nell'onActivityCreated del EvaluationFragment");

        evaluationAdapter = new EvaluationAdapter(this.getActivity(), 0, deviceList);

        Log.d("debugTime", "Qua il evaluationAdapter è inizializzato, se non è inizializzato questo log crasha " + evaluationAdapter);
        lv.setAdapter(evaluationAdapter);

        lv.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Log.d("debugTime","Sono entrato nell'onItemClickEvaluation");
                //Codice dell'Onclick
                CacheHitObject selectedItem = evaluationAdapter.getDevice(position);
                int percentualeCacheHit = selectedItem.getPercentuale();
                makeToast("La percentuale del CacheHit è: " + percentualeCacheHit + "%");
            }
        });
    }

    /* L'idea di base nella valutazione del cache hit sta nel registrare quali segmenti vengono presi dalla cache
    e inviati ai client e quali vengono scaricati da internet, in quanto mancanti nella cache, e inviati al client
    che li richiede
     */

    public int addConnectedDevice(CacheHitObject device){
        deviceList.add(device);
        int positionOfDevice = deviceList.indexOf(device);
        return positionOfDevice;
    }

    public void updateProgressDevice(final int positionOfDevice){
        Log.d("progressLogger", "Siamo in updateProgressDevice oon positionOfDevice: " +positionOfDevice);
        final CacheHitObject device = deviceList.get(positionOfDevice);
        //device.addProgress(6000);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d("connectionLogger", "Siamo nel runOnUiThread");
                /*
                ProgressBar bar = device.getProgressBar();
                if (bar != null) {
                    //bar.setProgress(0);
                    bar.setProgress(device.getProgress());
                    bar.invalidate();
                }
                */
                deviceList.get(positionOfDevice).addProgress(6000);
                evaluationAdapter.notifyDataSetChanged();
            }
        });
        notifyAdapter();
    }

    public void setMaxProgressDevice(int positionOfDevice, int maxValue){
        Log.d("progressLogger", "Settata lunghezza massima dispositivo: " + positionOfDevice + "maxValue: " + maxValue);
        CacheHitObject device = deviceList.get(positionOfDevice);
        /*
        ProgressBar bar = device.getProgressBar();
        if (bar != null) {
            bar.setMax(maxValue);
        }
        */
        if(device != null){
            device.setMaxProgress(maxValue);
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    evaluationAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    public void notifyAdapter(){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                evaluationAdapter.notifyDataSetChanged();
            }
        });
    }

    public void makeToast(String text){
        Context context = getContext();
        //CharSequence text = "Hello toast!";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    public CacheHitObject getCacheHitObject(int positionOfDevice){
        CacheHitObject retrieveCacheHitObject = deviceList.get(positionOfDevice);
        return retrieveCacheHitObject;
    }
}

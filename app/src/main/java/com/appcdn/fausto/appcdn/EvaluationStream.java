package com.appcdn.fausto.appcdn;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Created by Utente on 04/12/2017.
 */

public class EvaluationStream {

    public EvaluationStream(){

    }

    public void generateEvaluationText(Context context, EvaluationStreamFile evaluationStreamFile) {

        Log.d("connectionLogger", "Siamo nel generateEvaluationText");
        double percentage = evaluationStreamFile.getPercentage();
        String timestamp = evaluationStreamFile.getEndTimestamp();
        int cacheHitCounter = evaluationStreamFile.getCacheHitCounter();
        String nomeVideo = evaluationStreamFile.getNomeVideo();

        String sFileName = evaluationStreamFile.getNomeFile();
        String sBody = "Streaming effettuato su video: " + nomeVideo + "\n" + "Timestamp: " + timestamp + "\n" + " Cache hit: " + cacheHitCounter + "\n" + " Percentuale: " + percentage;

        try {
            File root = new File( context.getExternalFilesDir(null), "Evaluation");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, sFileName);
            if(!gpxfile.exists()){
                gpxfile.createNewFile();
            }

            writeToFile(gpxfile.toString(), sBody); //Da rivedere un secondo
/*
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
*/
            //Serve per rendere visibile via USB la cartella creata con i file al suo interno
            MediaScannerConnection.scanFile(context, new String[] {root.toString()}, null, null);

            //Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeToFile(/*Context context,*/ String pathName, String data) {
        try {
            File filePathName = new File(pathName);
            //OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(pathName, Context.MODE_PRIVATE));
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePathName));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
            e.printStackTrace();
        }
    }


}

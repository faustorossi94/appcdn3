package com.appcdn.fausto.appcdn;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Created by Utente on 04/12/2017.
 */

public class EvaluationStreamFile {

    private String nomeFile;
    private String timestampInizio;
    private double percentage;
    private String timestampFine;
    private int cacheHitCounter;
    private String nomeDispositivo;
    private String nomeVideo;
/*
    public EvaluationStreamFile(double percentage, long timestamp, int cacheHitCounter){
        this.percentage = percentage;
        this.timestamp = timestamp;
        this.cacheHitCounter = cacheHitCounter;
    }
*/

    public EvaluationStreamFile(String nomeDispositivo){
        this.nomeDispositivo = nomeDispositivo;
        this.nomeFile = createNomeFile(nomeDispositivo);
    }

    public String getNomeFile(){
        return this.nomeFile;
    }

    public void setNomeFile(String nomeFile){
        this.nomeFile = nomeFile;
    }

    public String getTimestampInizio(){
        return this.timestampInizio;
    }

    public void setTimestampInizio(String timestampInizio){
        this.timestampInizio = timestampInizio;
    }

    public double getPercentage(){
        return this.percentage;
    }

    public void setPercentage(double percentage){
        this.percentage = percentage;
    }

    public String getTimestampFine(){
        return this.timestampFine;
    }

    public void setTimestamp(String timestampFine){
        this.timestampFine = timestampFine;
    }

    public int getCacheHitCounter(){
        return this.cacheHitCounter;
    }

    public void setCacheHitCounter(int cacheHitCounter){
        this.cacheHitCounter = this.cacheHitCounter + cacheHitCounter;
    }

    public String getNomeDispositivo(){
        return this.nomeDispositivo;
    }

    public void setNomeDispositivo(){
        this.nomeDispositivo = nomeDispositivo;
    }

    public String getNomeVideo(){
        return this.nomeVideo;
    }

    public void setNomeVideo(String nomeVideo){
        this.nomeVideo = nomeVideo;
    }

    private String createNomeFile(String nomeDispositivo){
        timestampInizio = getSystemTimestamp();
        String nomeFileStream = "Streaming-" + nomeDispositivo + "-" + timestampInizio;
        return nomeFileStream;
    }

    public String getEndTimestamp(){
        timestampFine = getSystemTimestamp();
        return timestampFine;
    }

    private String getSystemTimestamp(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyy.MM.dd.HH.mm.ss");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String timestampFormat = sdf.format(timestamp);
        return timestampFormat;
    }
}

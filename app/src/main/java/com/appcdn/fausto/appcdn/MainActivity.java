package com.appcdn.fausto.appcdn;

import android.app.AlertDialog;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.net.wifi.p2p.WifiP2pManager.ChannelListener;
import android.net.wifi.p2p.WifiP2pManager.PeerListListener;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    //Pulsanti per la scelta del video da riprodurre
    Button big_buck_bunny = null;
    Button elephants_dream = null;
    Button forest_men = null;
    Button red_bull = null;
    Button tears_steel = null;
    Button swiss_account = null;
    Button valkaama = null;

    //Url generico del database ITEC
    private String serverUrl = "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/";

    //Url delle repository dove sono contenuti i video da scaricare
    private String serverBigBuckBunny = serverUrl + "BigBuckBunny/6sec/BigBuckBunny_6s_simple_2014_05_09.mpd";
    private String serverElephantsDream = serverUrl + "ElephantsDream/6sec/ElephantsDream_6s_simple_2014_05_09.mpd";
    private String serverForestsMen = serverUrl + "OfForestAndMen/6sec/OfForestAndMen_6s_simple_2014_05_09.mpd";
    private String serverRedBull = serverUrl + "RedBullPlayStreets/6sec/RedBull_6_simple_2014_05_09.mpd";
    private String serverTearsSteel = serverUrl + "TearsOfSteel/6sec/TearsOfSteel_6s_simple_2014_05_09.mpd";
    private String serverSwissAccount = serverUrl + "TheSwissAccount/6sec/TheSwissAccount_6s_simple_2014_05_09.mpd";
    private String serverValkaama = serverUrl + "Valkaama/6sec/Valkaama_6s_simple_2014_05_09.mpd";

    //String delle cartelle cache
    private String videoPathBigBuckBunny = "BigBuckBunny//6sec";
    private String videoPathElephantsDream = "ElephantsDream//6sec";
    private String videoPathForestsMen = "OfForestAndMen//6sec";
    private String videoPathRedBull = "RedBullPlayStreets//6sec";
    private String videoPathTearsSteel = "TearsOfSteel//6sec";
    private String videoPathSwissAccount = "TheSwissAccount//6sec";
    private String videoPathValkaama = "Valkaama//6sec";


    private List<WifiP2pDevice> peers = new ArrayList<WifiP2pDevice>();

    //Varibili per la gestione del WifiDirect
    WifiP2pManager mManager;
    Channel mChannel;
    WiFiDirectBroadcastReceiver mReceiver;

    IntentFilter mIntentFilter;

    int actionService;
    boolean isActivated = false;


    SharedWiFiManager sharedWiFiManager;
    SharedClientActivated sharedClientActivated;
    SharedServerActivated sharedServerActivated;
    SharedMainActivity sharedMainActivity;
    SharedEvaluationFragment sharedEvaluationFragment;

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    PeerListFragment peerListFragment;

    EvaluationFragment evaluationFragment;

    private PeerListListener peerListListener = new PeerListListener() {
        @Override
        public void onPeersAvailable(WifiP2pDeviceList peerList) {

            Log.d("debugTime","Siamo nell'onPeersAvailable");

            Collection<WifiP2pDevice> refreshedPeers = peerList.getDeviceList();
            if (!refreshedPeers.equals(peers)) {
                Log.d("debugTime","Siamo entrati nell'if");
                peers.clear();
                peers.addAll(refreshedPeers);

                peerListFragment.setDeviceList(peers);
                peerListFragment.setDeviceListAdapter(peers);
                peerListFragment.notifyChange();
            }

            if (peers.size() == 0) {
                Log.d("debugTime", "No devices found");
                return;
            }
        }
    };

    //Metodo per aprire la VideoPlayerActivity e inviare un valore che corrisponde al video scelto
    private void openVideoActivity(String uriVideo, String videoPath) {
        Intent intent = new Intent(this, VideoPlayerActivity.class);
        intent.putExtra("uriVideo", uriVideo);
        intent.putExtra("videoPath", videoPath);
        intent.putExtra("clientActivated", sharedClientActivated.getClientActivated());
        startActivity(intent);
    }

    private void startDeviceConnectionServiceServer() {
        //Is Server or stop service
        Log.d("debugTime","Metodo startDeviceConnectionServiceServer eseguito correttamente in MainActivity");
        actionService = 0;
        Intent intent = new Intent(this, DeviceConnectionService.class);
        intent.putExtra("actionService", actionService);
        intent.putExtra("sourceIntent", "MainActivity.class");
        Log.d("debugTime","L'actionSerivce = " + actionService);
        startService(intent);
    }

    private void stopDeviceConnectionService() {
        Log.d("debugTime","Metodo stopDeviceConnectionService eseguito correttamente in MainActivity");
        actionService = 2;
        Intent intent = new Intent(this, DeviceConnectionService.class);
        intent.putExtra("actionService", actionService);
        intent.putExtra("sourceIntent", "MainActivity.class");
        startService(intent);
        isActivated = false;
        makeToast("Service spento");
    }

    public void buttonStartServer(){
        if(!isActivated){
            startDeviceConnectionServiceServer();
            makeToast("Server attivato");
        }
        else{
            makeToast("Server già avviato");
        }
    }

    //Metodo che permette il collegamento di un app menu in questa activity
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_start_server:

                Log.d("debugTime","Pulsante server premuto");

                //Codice per aprire e gestire il service che permette al dispositivo di essere considerato un server
                buttonStartServer();
                return true;

            case R.id.action_informations:

                //Codice per aprire una finestra con le informazioni su chi ha realizzato l'applicazione
                openInformationsDialog();
                return true;

            case R.id.action_stop_service:


                Log.d("debugTime", "clientActivaed " + sharedClientActivated.getClientActivated());

                //Codice per stoppare il foreground service presente
                stopDeviceConnectionService();

            case R.id.action_start_client:

                //Codice per avviare la modalità client del dispositivo
                //Al click oltre che settare la variabile clientActivated bisogna aprire il fragment dei peer

                discoverPeers();
                makeToast("discoverPeers() attivato");
                Log.d("debugTime", "clientActivated " + sharedClientActivated.getClientActivated());

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    //Nel server viene sostituito il fragment che mostra i peer disponibili con uno che mostra i peer a cui ci si connette
    public void replaceFragment(){
        fragmentTransaction.replace(R.id.peer_fragment, evaluationFragment);
        fragmentTransaction.commit();
    }


    public void openInformationsDialog() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle(R.string.app_bar_informations);

        //Mostra dialog delle informazioni
        TextView message = new TextView(this);
        message.setText(R.string.informations_text);
        message.setGravity(Gravity.CENTER_HORIZONTAL);
        alertDialogBuilder.setView(message);

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void makeToast(String text){
        Context context = getApplicationContext();
        //CharSequence text = "Hello toast!";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    public void inizializzazioneWiFiDirect(){
        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mManager.initialize(this, getMainLooper(), new ChannelListener() {
            @Override
            public void onChannelDisconnected() {
                inizializzazioneWiFiDirect();
            }
        });
        mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this, peerListListener);
    }

    public void discoverPeers(){
        mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                //Cosa succede se si trovano dei peer
                Log.d("debugTime", "discoverPeers() Success");
            }

            @Override
            public void onFailure(int reasonCode) {
                //Cosa succede se non si trovano dei peer
                Log.d("debugTime", "discoverPeers() Failure");
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        //Inizializzazione e funzionamento dei pulsanti di scelta del video da riprodurre
        big_buck_bunny = (Button) findViewById(R.id.big_buck_bunny);
        elephants_dream = (Button) findViewById(R.id.elephants_dream);
        forest_men = (Button) findViewById(R.id.forest_men);
        red_bull = (Button) findViewById(R.id.red_bull);
        tears_steel = (Button) findViewById(R.id.tears_steel);
        swiss_account = (Button) findViewById(R.id.swiss_account);
        valkaama = (Button) findViewById(R.id.valkaama);
        //Implementazione logica pulsanti
        big_buck_bunny.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                openVideoActivity(serverBigBuckBunny, videoPathBigBuckBunny);
            }
        });
        elephants_dream.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                openVideoActivity(serverElephantsDream, videoPathElephantsDream);
            }
        });
        forest_men.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                openVideoActivity(serverForestsMen, videoPathForestsMen);
            }
        });
        red_bull.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                openVideoActivity(serverRedBull, videoPathRedBull);
            }
        });
        tears_steel.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                openVideoActivity(serverTearsSteel, videoPathTearsSteel);
            }
        });
        swiss_account.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                openVideoActivity(serverSwissAccount, videoPathSwissAccount);
            }
        });
        valkaama.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                openVideoActivity(serverValkaama, videoPathValkaama);
            }
        });

        inizializzazioneWiFiDirect();

        sharedWiFiManager = new SharedWiFiManager();
        sharedWiFiManager.set(mManager, mChannel, mReceiver);

        sharedClientActivated = new SharedClientActivated();
        sharedClientActivated.setClientActivated(false);

        sharedServerActivated = new SharedServerActivated();
        sharedServerActivated.setServerActivated(false);

        sharedMainActivity = new SharedMainActivity();
        sharedMainActivity.setMainActivity(this);

        evaluationFragment = new EvaluationFragment();

        sharedEvaluationFragment = new SharedEvaluationFragment();
        sharedEvaluationFragment.setEvaluationFragment(evaluationFragment);

        peerListFragment = new PeerListFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        getSupportFragmentManager().beginTransaction().add(R.id.peer_fragment, peerListFragment, "peer_fragment_tag").commit();

    }

    protected void onStart() {
        super.onStart();
    }

    protected void onResume() {
        super.onResume();
        discoverPeers();
        registerReceiver(mReceiver, mIntentFilter);
    }

    protected void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);
    }

    protected void onDestroy() {
        super.onDestroy();
        stopDeviceConnectionService();
    }

    public static class SharedWiFiManager {
        private static WifiP2pManager mManager;
        private static WifiP2pManager.Channel mChannel;
        private static WiFiDirectBroadcastReceiver mReceiver;

        static public void set(WifiP2pManager mManager, WifiP2pManager.Channel mChannel, WiFiDirectBroadcastReceiver mReceiver){
            SharedWiFiManager.mManager = mManager;
            SharedWiFiManager.mChannel = mChannel;
            SharedWiFiManager.mReceiver = mReceiver;
        }

        static public WifiP2pManager getManager(){
            return mManager;
        }

        static public WifiP2pManager.Channel getChannel(){
            return mChannel;
        }

        static public WiFiDirectBroadcastReceiver getReceiver(){
            return mReceiver;
        }

    }

    public static class SharedClientActivated {
        private static boolean clientActivated = false;

        static public void setClientActivated(boolean clientActivated){
            SharedClientActivated.clientActivated = clientActivated;
        }

        static public boolean getClientActivated(){
            return clientActivated;
        }
    }

    public static class SharedServerActivated {
        private static boolean serverActivated = false;

        static public void setServerActivated(boolean serverActivated) {
            SharedServerActivated.serverActivated = serverActivated;
        }

        static public boolean getServerActivated() {
            return serverActivated;
        }
    }

    public static class SharedMpdDirectory {
        private static File mpdDirectory = new File("data//data//com.appcdn.fausto.appcdn//cache");

        public static void setMpdDirectory(File mpdDirectory){
            SharedMpdDirectory.mpdDirectory = mpdDirectory;
        }

        public static File getMpdDirectory(){
            return SharedMpdDirectory.mpdDirectory;
        }
    }

    public static class SharedPeerListFragment{
        private static PeerListFragment peerListFragment;

        public static void setPeerListFragment(PeerListFragment peerListFragment){
            SharedPeerListFragment.peerListFragment = peerListFragment;
        }

        public static PeerListFragment getPeerListFragment(){
            return SharedPeerListFragment.peerListFragment;
        }
    }

    public static class SharedEvaluationFragment{
        private static EvaluationFragment evaluationFragment;

        public static void setEvaluationFragment(EvaluationFragment evaluationFragment){
            SharedEvaluationFragment.evaluationFragment = evaluationFragment;
        }

        public static EvaluationFragment getEvaluationFragment(){
            return SharedEvaluationFragment.evaluationFragment;
        }
    }

    public static class SharedMainActivity{
        private static MainActivity mainActivity;

        public static void setMainActivity(MainActivity mainActivity){
            SharedMainActivity.mainActivity = mainActivity;
        }

        public static MainActivity getMainActivity(){
            return SharedMainActivity.mainActivity;
        }
    }
}

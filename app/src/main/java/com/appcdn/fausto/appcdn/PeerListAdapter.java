package com.appcdn.fausto.appcdn;

import android.content.Context;
import android.net.wifi.p2p.WifiP2pDevice;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utente on 03/08/2017.
 */

public class PeerListAdapter extends ArrayAdapter<WifiP2pDevice> {

    LayoutInflater mInflater;
    List<WifiP2pDevice> deviceList = new ArrayList<>();
    Context context;

    public PeerListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<WifiP2pDevice> objects) {
        super(context, resource, objects);

        Log.d("debugTime","Siamo nel costruttore del PeerListAdapter");


        mInflater = LayoutInflater.from(context);
        deviceList = objects;
        this.context = context;
    }

    public void setDeviceList(List<WifiP2pDevice> deviceList){
        this.deviceList = deviceList;
    }

    @Override
    public WifiP2pDevice getItem(int position) {
        return getDevice(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void notifyDataSetChanged() {
        // TODO Auto-generated method stub
        super.notifyDataSetChanged();
    }

    @Override
    public int getCount(){
        return deviceList.size();
    }

    public WifiP2pDevice getDevice(int position){
        return deviceList.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        Log.d("debugTime","Siamo nel getView del PeerListAdapter");
        // TODO Auto-generated method stub
        ViewHolder holder;
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.peer_device_item, null);
            holder = new ViewHolder();
            if(getDevice(position) != null) {
                Log.d("debugTime", "Siamo nell'if dove estraiamo il nome e l'indirizzo del dispositivo");
                holder.deviceName = (TextView) convertView.findViewById(R.id.device_name);
                holder.deviceAddress = (TextView) convertView.findViewById(R.id.device_address);
            }

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(getDevice(position)!= null) {
            Log.d("debugTime","Settiamo i dati del dispositivo nel setText");
            holder.deviceName.setText(deviceList.get(position).deviceName);
            holder.deviceAddress.setText(deviceList.get(position).deviceAddress);
        }
        return convertView;
    }

    static class ViewHolder{
        TextView deviceName;
        TextView deviceAddress;
    }
}

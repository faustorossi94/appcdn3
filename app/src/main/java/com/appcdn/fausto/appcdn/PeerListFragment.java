package com.appcdn.fausto.appcdn;

import android.content.Intent;
import android.net.wifi.p2p.WifiP2pDevice;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utente on 03/08/2017.
 */

public class PeerListFragment extends Fragment {

    private List<WifiP2pDevice> deviceList = new ArrayList<WifiP2pDevice>();
    PeerListAdapter peerListAdapter;
    ListView lv;
    WifiP2pDevice host;
    View peerClicked;

    public void startClientConnection(WifiP2pDevice host){
        Log.d("debugTime","startClientConnection del Fragment correttamente attivato");
        Intent intent = new Intent(getActivity(),DeviceConnectionService.class);
        intent.putExtra("actionService", 1);
        intent.putExtra("host", host);
        intent.putExtra("sourceIntent", "MainActivity.class");
        getActivity().startService(intent);
    }

    public void setDeviceList(List<WifiP2pDevice> deviceList){
        this.deviceList = deviceList;
    }

    public void setDeviceListAdapter(List<WifiP2pDevice> deviceList){
        peerListAdapter.setDeviceList(deviceList);
    }

    public void notifyChange(){
        peerListAdapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d("debugTime","Siamo nell'onCreateView del fragment");

        View rootView = inflater.inflate(R.layout.peer_list_fragment, container, false);

        lv = (ListView)rootView.findViewById(R.id.peer_list);

        return rootView;
    }

    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        Log.d("debugTime","Siamo nell'onActivityCreated del Fragment");

        peerListAdapter = new PeerListAdapter(this.getActivity(), 0, deviceList);

        Log.d("debugTime", "Qua il peerListAdapter è inizializzato, se non è inizializzato questo log crasha " + peerListAdapter);
        lv.setAdapter(peerListAdapter);

        lv.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Log.d("debugTime","Sono entrato nell'onItemClick");
                //Codice dell'Onclick
                peerClicked = view;
                host = peerListAdapter.getDevice(position);
                Log.d("debugTime","L'host in PeerListFragment è " + host.deviceName);
                startClientConnection(host);
            }
        });
    }

    private void setColorListItem(View view){
        TextView deviceNameText = (TextView) view.findViewById(R.id.device_name);
        TextView deviceAddressText = (TextView) view.findViewById(R.id.device_address);

        deviceNameText.setTextColor(ContextCompat.getColor(getContext(), R.color.connection_status));
        deviceAddressText.setTextColor(ContextCompat.getColor(getContext(), R.color.connection_status));
    }

    public void setConnectionColor(){
        setColorListItem(peerClicked);
    }


}

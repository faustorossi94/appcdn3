package com.appcdn.fausto.appcdn;

import java.io.Serializable;

/**
 * Created by Utente on 28/08/2017.
 */

public class ResponseObject implements Serializable {

    private static final long serialVersionUID = 1L;

    String type;
    String typeOfFile;
    String cacheSpanName;
    String cacheSpanKey;
    byte[] cacheSpanFileBytes;
    int position;
    long timeStamp;


    public ResponseObject(String type, String typeOfFile, String cacheSpanName, String cacheSpanKey, byte[] cacheSpanFileBytes, int position, long timeStamp){
        this.type = type;
        this.typeOfFile = typeOfFile;
        this.cacheSpanName = cacheSpanName;
        this.cacheSpanKey = cacheSpanKey;
        this.cacheSpanFileBytes = cacheSpanFileBytes;
        this.position = position;
        this.timeStamp = timeStamp;
    }

    public void setType(String type){
        this.type = type;
    }

    public String getType(){
        return this.type;
    }

    public void setTypeOfFile(String typeOfFile){
        this.typeOfFile = typeOfFile;
    }

    public String getTypeOfFile(){
        return this.typeOfFile;
    }

    public void setCacheSpanName(String cacheSpanName){
        this.cacheSpanName = cacheSpanName;
    }

    public String getCacheSpanName(){
        return this.cacheSpanName;
    }

    public void setCacheSpanKey(String cacheSpanKey){
        this.cacheSpanKey = cacheSpanKey;
    }

    public String getCacheSpanKey(){
        return cacheSpanKey;
    }

    public void setCacheSpanFileBytes(byte[] cacheSpanFileBytes){
        this.cacheSpanFileBytes = cacheSpanFileBytes;
    }

    public byte[] getCacheSpanFileBytes(){
        return this.cacheSpanFileBytes;
    }

    public void setCacheSpanPosition(int position){
        this.position = position;
    }

    public int getCacheSpanPosition(){
        return this.position;
    }

    public void setTimeStamp(long timeStamp){
        this.timeStamp = timeStamp;
    }

    public long getTimeStamp(){
        return this.timeStamp;
    }

}

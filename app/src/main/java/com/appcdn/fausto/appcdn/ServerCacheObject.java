package com.appcdn.fausto.appcdn;

import java.io.Serializable;

/**
 * Created by Utente on 03/12/2017.
 */

public class ServerCacheObject implements Serializable {

    private String positionInServer;
    private String keyOfFile;
    private String videoPath;
    private String fileName;
    private byte[] fileBytes;

    public ServerCacheObject(String positionInServer, String keyOfFile, String videoPath, String fileName, byte[] fileBytes){
        this.positionInServer = positionInServer;
        this.keyOfFile = keyOfFile;
        this.videoPath = videoPath;
        this.fileName = fileName;
        this.fileBytes = fileBytes;
    }

    public String getPositionInServer(){
        return this.positionInServer;
    }

    public String getKeyOfFile(){
        return this.keyOfFile;
    }

    public String getVideoPath(){
        return this.videoPath;
    }

    public String getFileName(){
        return this.fileName;
    }

    public byte[] getFileBytes(){
        return this.fileBytes;
    }
}

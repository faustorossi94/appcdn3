package com.appcdn.fausto.appcdn;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by Utente on 07/07/2017.
 */

public class SplashActivity extends Activity {

    public void nextActivity(){
        final Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        final ImageView imageView = (ImageView) findViewById(R.id.splash_image);
        imageView.setOnTouchListener(new View.OnTouchListener(){
            public boolean onTouch(View view, MotionEvent motionEvent){
                nextActivity();
                return false;
            }
        });
    }

    public void onStart(){
        super.onStart();
    }

    public void onDestroy(){
        super.onDestroy();
    }
}

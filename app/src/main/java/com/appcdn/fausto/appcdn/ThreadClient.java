package com.appcdn.fausto.appcdn;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheUtil;
import com.google.android.exoplayer2.upstream.cache.CacheUtil.CachingCounters;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;

import static android.util.Log.d;

/**
 * Created by Utente on 21/08/2017.
 */

public class ThreadClient extends Thread implements Runnable {
    Object clientLock = new Object();
    String chunkKey = null;
    SimpleCache simpleCache;
    boolean stop = false;
    String groupOwnerAddress;
    JSONObject requestData;
    Context context;
    String videoPath;
    Socket socket;
    OutputStream os;
    ObjectOutputStream oos;
    LeastRecentlyUsedCacheEvictor evictor = new LeastRecentlyUsedCacheEvictor(100 * 1024 * 1024);
    File cacheDir;
    InputStream is = null;
    ObjectInputStream ois = null;
    DataSpec dataFromServer;
    CreateDirectory createDirectory;
    File mpdDirectory;
    //int sec;

    MainActivity.SharedMpdDirectory sharedMpdDirectory;

    VideoPlayerActivity.NotifyClass notifyClass;

    public ThreadClient(Context context, String groupOwnerAddress){
        this.groupOwnerAddress = groupOwnerAddress;
        this.context = context;

        //Creazione se non presente della cartella dove sistemiamo gli mpd locali
        sharedMpdDirectory = new MainActivity.SharedMpdDirectory();
        mpdDirectory = sharedMpdDirectory.getMpdDirectory();
        mpdDirectoryCreator(mpdDirectory);

        notifyClass = new VideoPlayerActivity.NotifyClass();


    }

    public void mpdDirectoryCreator(File mpdDirectoryFile){
        createDirectory = new CreateDirectory();
        createDirectory.directoryCreator(mpdDirectoryFile);
    }

    public void wakeUp(String chunkKeyFromExoplayer, String videoPathFromExoplayer /*long realDurationMillis*/){
        chunkKey = chunkKeyFromExoplayer;
        videoPath = videoPathFromExoplayer;
        //this.realDurationMillis = realDurationMillis;
/*
        double secNotRound = realDurationMillis/1000;
        Double secDouble = new Double(secNotRound);
        sec = secDouble.intValue();
*/
        setCache(videoPathFromExoplayer);

        synchronized (clientLock){
            clientLock.notify();
        }
    }

    public void setCache(String videoPath){
        cacheDir = new File(context.getCacheDir(), videoPath);
        simpleCache = new SimpleCache(cacheDir, evictor);
    }

    public boolean checkIsHttp(String stringToCheck){
        boolean isHttp = false;
        String[] stringSplit = stringToCheck.split("/");
        if(stringSplit[1].equals("http:")){
            isHttp = true;
        }
        return isHttp;
    }

    public void setRequest(String cacheSpanKey, String videoPath /*long realDurationMillis*/){
        Log.d("connectionLogger", "Sono nel setRequest");
        try{
            requestData = new JSONObject();
            requestData.put("type", "REQUEST");
            requestData.put("cacheSpanKey", cacheSpanKey);
            requestData.put("videoPath", videoPath);
            //requestData.put("durataVideo", realDurationMillis);
            Log.d("connectionLogger", "RequestData settato con type REQUEST, cacheSpanKey " + cacheSpanKey + " e videoPath " + videoPath);
        }
        catch (JSONException e){
            Log.d("connectionLogger","Errore nella creazione della request (Lato client)");
        }
    }

    public byte[] readBytesFromFile(File file){
        //init array with file length
        byte[] bytesArray = new byte[(int) file.length()];

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            fis.read(bytesArray); //read file into bytes[]
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bytesArray;
    }

    public void closeConnection(){
        if (socket != null) {
            if (socket.isConnected()) {
                try {
                    socket.close();
                    stop = true;
                    //Thread.currentThread().isInterrupted();
                } catch (IOException e) {
                    //catch logic
                    d("connectionLogger", "Exception del finally con messaggio: " + e.getMessage());
                    stop = true;
                }
            }
        }
    }

    public boolean cachingChunk(DataSpec dataSpec) {

        boolean isOk = false;

        DefaultDataSourceFactory defaultDataSourceFactory = new DefaultDataSourceFactory(context, "Exoplayer");
        CacheDataSource cacheDataSource = new CacheDataSource(simpleCache, defaultDataSourceFactory.createDataSource());
        CacheUtil.CachingCounters cachingCounters = new CachingCounters();
        byte[] buffer = new byte[1024];
        try {
            CacheUtil.cache(dataSpec, simpleCache, cacheDataSource, cachingCounters);
            isOk = true;
            return isOk;
        } catch (Exception e) {
            Log.d("connectionLogger", "Eccezione successa in cachingChunk");
            e.printStackTrace();
            isOk = false;
            return isOk;
        }
    }


    public boolean saveMpdInCache(byte[] mpdBytes, String mpdFileName){


        //Metodo per salvare l'mpd in cache
        File cacheMpdFile = new File(mpdDirectory + "//" + mpdFileName);

        if (!cacheMpdFile.exists()) {
            try {
                cacheMpdFile.createNewFile();
            }
            catch (IOException e){
                Log.d("connectionLogger", "Errore nella creazione del file mpd da scrivere");
            }
        }

        boolean isOk = false;

        try {
            FileOutputStream fos = new FileOutputStream(cacheMpdFile);
            fos.write(mpdBytes);
            fos.close();
            isOk = true;
            return isOk;
        }
        catch (Exception e){
            Log.d("connectionLogger", "Errore nel savataggio del mpd lato client nella cartella mpds");
            isOk = false;
            return isOk;
        }
    }
/*
    public String estraiNomeMpd(String uriMpdVideo){
        String uriMpdEstratto;
        uriMpdEstratto = uriMpdVideo.substring(uriMpdVideo.indexOf(8));
        return uriMpdEstratto;
    }
*/
    @Override
        public void run() {
        socket = new Socket();
        try {
            socket.setReuseAddress(true);
            socket.bind(null);
            socket.connect((new InetSocketAddress(groupOwnerAddress, 8888)), 7000); //Qui va l'host che rappresenta il server di destinazione, la port la porta di uscita e il numero che rappresenta il timeout

            os = socket.getOutputStream();
            oos = new ObjectOutputStream(new BufferedOutputStream(os));

            //Prima di entrare nel while bisogna inviare le informazioni del dispositivo al server
            String deviceName = Build.MODEL;
            oos.writeObject(deviceName);
            oos.flush();

            //Forse qui si necessita di un nuovo os e oos per la sincronizzazione


        d("connectionLogger", "Client Thread Avviato");
        while (!stop) {
            //Send the message to the server

            d("connectionLogger", "Siamo nel while del clientThread");
            synchronized (clientLock) {
                try {
                    clientLock.wait();
                }
                catch (InterruptedException e){
                    Log.d("connectionLogger", "InterruptException attivato");
                }
                Log.d("connectionLogger","Lock superato quindi exoplayer necessita di un segmento");
                //Il thread va in loop fino a che non si decide di fermarlo
                try {
                    /**
                     * Create a client socket with the host,
                     * port, and timeout information.
                     */

                    //////////////////INVIO DEL JSON AL SERVER


                    //Qui va il codice che ottiene il pacchetto mancante da exoplayer e riempie il json
                    String chunkKeyToSend = chunkKey;
                    String videoPathToSend = videoPath;
                    //long realDurationMillisToSend = sec;
                    setRequest(chunkKeyToSend, videoPathToSend /*realDurationMillisToSend*/);

                    try {
                        oos.writeObject(requestData.toString());
                        oos.flush();
                        Log.d("connectionLogger", "ObjectOutputStream oos ha inviato i dati");
                    }
                    catch (SocketException e){
                        oos.flush();
                        //oos.close();
                        Log.d("connectionLogger", "Disconnessione avvenuta lato thread client durante l'invio della richiesta");
                    }
                    catch (Exception e){
                        Log.d("connectionLogger", "Errore Json Trovato");
                        e.printStackTrace();
                    }

                    //////////////OTTENIMENTO JSON DAL SERVER

                    //Get the return message from the server
                    try {
                        is = socket.getInputStream();
                        ois = new ObjectInputStream(new BufferedInputStream(is));
                        ResponseObject responseObject = (ResponseObject) ois.readObject();
                        if (responseObject != null) {
                            String type = responseObject.getType();
                            if (type.equals("RESPONSE")) {
                                byte[] cacheSpanFileBytes = responseObject.getCacheSpanFileBytes();
                                long cacheSpanFileNumberBytes = cacheSpanFileBytes.length;

                                String typeOfFile = responseObject.getTypeOfFile();
                                if(typeOfFile.equals("mpd")){
                                    //Il file tornato è un mpd e quindi va sistemato in cache
                                    String mpdFileName = responseObject.getCacheSpanName();

                                    //Questo rimuove l'http:// dal nome del mpd che stiamo cercando di salvare offline
                                    //String cacheSpanKeyNoHttp = estraiNomeMpd(cacheSpanKey);
                                    //Questo metodo salva offline l'mpd scaricato
                                    boolean mpdInCache = saveMpdInCache(cacheSpanFileBytes, mpdFileName);
                                    if(mpdInCache){
                                        //notifyMpd.setTrue();
                                        notifyClass.setMpdTrue();
                                        Log.d("connectionLogger", "Mpd salvato correttamente in cache");
                                    }
                                }

                                else {
                                    //Alcune variabili del responseObject non le uso. Se servissero in futuro le posso inserire successivamente

                                    String fileName = responseObject.getCacheSpanName();

                                    Log.d("connectionLogger", "Il numero di byte ottenuto dal server è:" + cacheSpanFileNumberBytes);

                                    File cacheSpanFile = new File(context.getFilesDir().getPath().toString(), fileName);

                                    FileOutputStream fos = new FileOutputStream(cacheSpanFile);
                                    fos.write(cacheSpanFileBytes);
                                    fos.close();

                                    byte[] bytesArray = readBytesFromFile(cacheSpanFile);
                                    long numberOfBytes = bytesArray.length;
                                    Log.d("connectionLogger", "Il numero di byte del file temporaneo scritto cacheSpanFile è: " + numberOfBytes);

                                    String cacheSpanKey = responseObject.getCacheSpanKey();
                                    int position = responseObject.getCacheSpanPosition();

                                    dataFromServer = new DataSpec(Uri.fromFile(cacheSpanFile), position, cacheSpanFileNumberBytes, cacheSpanKey);

                                    boolean isCached = cachingChunk(dataFromServer);

                                    //Giusto per appuntare che l'mpd viene salvato sia come chunk sia come mpd effettivo nella cartella mpds per sicurezza
                                    //Noi teniamo conto dei file mpd salvati in mpds
                                    if(isCached){
                                        //notifyChunk.setTrue();
                                        notifyClass.setChunkTrue();
                                        Log.d("connectionLogger", "Chunk salvato correttamente in cache");
                                    }

                                    //Nel momento in cui metto in cache il pacchetto richiesto deve esserci una notifica da parte del thread che avvisa che il pacchetto è disponibile
                                    //Quando il pacchetto è disponibile bisogna vedere come inizializzare il tutto di nuovo dinamicamente per far leggere il pacchetto al player
                                    //Come notifica possiamo pensare anche di inserire una classe costante dentro il video player e gestirla da qui
                                    //Una classe costante con un booleano dentro che con true indica che il pacchetto è disponibile e con false non è disponibile
                                    //Bisogna vedere come controllare sempre in loop lo stato del booleano e muoversi di conseguenza
                                    //Bisogna capire come dire ad exoplayer di leggere il pacchetto nuovo

                                    Log.d("connectionLogger", "I file in cache sono: " + simpleCache.getKeys());
                                }
                            }
                        } else {
                            Log.d("connectionLogger", "Errore il client sta processando un file REQUEST invece di RESPONSE");
                        }
                    } catch (Exception e) {
                        Log.d("connectionLogger", "Qualcosa è successo nel client");
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    //catch logic
                    d("connectionLogger", "Exception con messaggio: " + e.getMessage());
                    e.printStackTrace();
                    stop = true;
                }

            }
        }
        } catch (SocketException e) {
            e.printStackTrace();
            Log.d("connectionLogger", "SocketException in setReuseAddress");
        }
        catch (IOException e){
            Log.d("connectionLogger", "IOException in socket bind e socket connect");
            e.printStackTrace();
        }

        //Fuori dal while
        /**
         * Clean up any open sockets when done
         * transferring or if an exception occurred.
         */ finally {
            Log.d("connectionLogger", "Siamo nel finally qualcosa è successo");
            if (socket != null) {
                if (socket.isConnected()) {
                    try {
                        stop = true;
                        os.close();
                        oos.close();
                        is.close();
                        ois.close();
                        closeConnection();
                    } catch (IOException e) {
                        //catch logic
                        d("connectionLogger", "Exception del finally con messaggio: " + e.getMessage());
                        stop = true;
                    }
                }
            }
        }
    }
}

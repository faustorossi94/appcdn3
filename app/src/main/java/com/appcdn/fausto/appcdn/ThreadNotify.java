package com.appcdn.fausto.appcdn;

import android.content.Context;
import android.util.Log;

/**
 * Created by Utente on 16/10/2017.
 */

public class ThreadNotify extends Thread implements Runnable {

    private Context context;
    private VideoPlayerActivity.NotifyClass notifyClass;
    private boolean stop = false;
    private VideoPlayerActivity videoPlayerActivity;


    public ThreadNotify(Context context, VideoPlayerActivity.NotifyClass notifyClass, VideoPlayerActivity videoPlayerActivity){
        this.context = context;
        this.notifyClass = notifyClass;
        this.videoPlayerActivity = videoPlayerActivity;
    }

    @Override
    public void run() {
        Log.d("notifyThread", "Il ThreadNotify si è correttamente avviato");
        stop = false;
        while(!stop){
            //L'mpd è arrivato
            if(notifyClass.getIsArrivedMpd() == true){
                Log.d("notifyThread", "Un mpd è arrivato");
                videoPlayerActivity.threadUseSetLocalMpd();
                notifyClass.setMpdFalse();
            }

            //Il chunk è arrivato
            if(notifyClass.getIsArrivedChunk() == true){
                Log.d("notifyThread", "Un chunk è arrivato e quindi avviamo il reBuild del videoPlayer");
                //videoPlayerActivity.threadUseReBuild();
                notifyClass.setChunkFalse();
            }
        }
    }

    public void stopNotifyThread(){
        Log.d("notifyThread", "ThreadNotify stoppato");
        this.stop = true;
    }
}

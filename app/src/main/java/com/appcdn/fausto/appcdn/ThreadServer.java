package com.appcdn.fausto.appcdn;

import android.content.Context;
import android.util.Log;

import com.appcdn.fausto.appcdn.DeviceConnectionService.SharedEvaluationStream;
import com.google.android.exoplayer2.upstream.cache.CacheSpan;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.util.NavigableSet;
import java.util.Set;

import static android.util.Log.d;

/**
 * Created by Utente on 21/08/2017.
 */

public class ThreadServer extends Thread implements Runnable {

    boolean stop = false;

    SimpleCache simpleCache;
    ResponseObject responseData;
    Context context;
    Socket clientSocket;
    LeastRecentlyUsedCacheEvictor evictor = new LeastRecentlyUsedCacheEvictor(100 * 1024 * 1024);
    File cacheDir;
    ObjectInputStream ois;
    String videoPath;
    String cacheSpanKey;
    String cacheSpanKeyToSend;
    Object serverLock = new Object();
    InputStream inputstream = null;
    OutputStream os = null;
    ObjectOutputStream oos = null;
    File mpdDirectory;
    CreateDirectory createDirectory;
    File tempDirectory;
    String originalKey;
    File tempCacheDirectory;

    MainActivity.SharedMpdDirectory sharedMpdDirectory;

    MainActivity.SharedEvaluationFragment sharedEvaluationFragment;
    EvaluationFragment evaluationFragment;

    MainActivity.SharedMainActivity sharedMainActivity;
    MainActivity mainActivity;

    CacheHitObject cacheHitObject;

    int positionOfDevice;

    EvaluationStreamFile evaluationStreamFile;

    DeviceConnectionService.SharedEvaluationStream sharedEvaluationStream;
    EvaluationStream evaluationStream;

    public ThreadServer(Context context, Socket clientSocket){
        this.context = context;
        this.clientSocket = clientSocket;

        //Creazione se non presente della cartella dove sistemiamo gli mpd locali
        sharedMpdDirectory = new MainActivity.SharedMpdDirectory();
        mpdDirectory = sharedMpdDirectory.getMpdDirectory();
        mpdDirectoryCreator(mpdDirectory);

        //Creazione della directory temporanea dove scaricare i file m4s
        tempDirectory = new File("data//data//com.appcdn.fausto.appcdn//temp");
        createDirectory.directoryCreator(tempDirectory);

        //Creazione della directory temporanea dove impostare una cache parallela
        tempCacheDirectory = new File("data//data//com.appcdn.fausto.appcdn//serverCache");
        createDirectory.directoryCreator(tempCacheDirectory);

        sharedEvaluationFragment = new MainActivity.SharedEvaluationFragment();
        evaluationFragment = sharedEvaluationFragment.getEvaluationFragment();

        sharedMainActivity = new MainActivity.SharedMainActivity();
        mainActivity = sharedMainActivity.getMainActivity();

        sharedEvaluationStream = new SharedEvaluationStream();
        evaluationStream = sharedEvaluationStream.getEvaluationStream();

    }

    public void stopServerThread(){
        stop = true;
    }

    public void addCacheHitToFragment(CacheHitObject cacheHitObject){
        positionOfDevice = evaluationFragment.addConnectedDevice(cacheHitObject);
        mainActivity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                evaluationFragment.notifyAdapter(); //Questo metodo genera errore nel worker thread
            }
        });
    }

    public void mpdDirectoryCreator(File mpdDirectoryFile){
        createDirectory = new CreateDirectory();
        createDirectory.directoryCreator(mpdDirectoryFile);
    }

    public byte[] readByteFromFile(File file){
        //init array with file length
        byte[] bytesArray = new byte[(int) file.length()];

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            fis.read(bytesArray); //read file into bytes[]
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bytesArray;
    }

    public ResponseObject setResponse(CacheSpan cacheSpan){
        Log.d("connectionLogger", "Sono nel setResponse");

        String type = "RESPONSE";
        File cacheSpanFile = cacheSpan.file;
        String cacheSpanName = cacheSpanFile.getName();
        String typeOfFile = typeOfFile(cacheSpanName);

        String[] cacheSpanNameSubstring = cacheSpanName.split("\\.");
        int position = Integer.parseInt(cacheSpanNameSubstring[1]);
        long timeStamp = Long.parseLong(cacheSpanNameSubstring[2]);


        byte[] bytesArray = readByteFromFile(cacheSpanFile);
        long numberOfBytes = bytesArray.length;

        Log.d("connectionLogger", "Il numero di byte del file è: " + numberOfBytes);

        responseData = new ResponseObject(type, typeOfFile, cacheSpanName, cacheSpanKeyToSend, bytesArray, position, timeStamp);

        return responseData;
    }

    public ResponseObject setChunkResponse(File chunkFile){
        Log.d("connectionLogger", "Sono nel setChunkResponse");

        String type = "RESPONSE";
        File cacheSpanFile = chunkFile;
        String cacheSpanName = cacheSpanFile.getName();
        String typeOfFile = typeOfFile(cacheSpanName);

        String[] cacheSpanNameSubstring = cacheSpanName.split("\\.");
        int position = Integer.parseInt(cacheSpanNameSubstring[1]);
        long timeStamp = Long.parseLong(cacheSpanNameSubstring[2]);


        byte[] bytesArray = readByteFromFile(cacheSpanFile);
        long numberOfBytes = bytesArray.length;

        Log.d("connectionLogger", "Il numero di byte del file è: " + numberOfBytes);

        responseData = new ResponseObject(type, typeOfFile, cacheSpanName, cacheSpanKeyToSend, bytesArray, 0, 0);

        return responseData;
    }

    public ResponseObject setMpdResponse(File mpdFile) {
        Log.d("connectionLogger", "Sono nel mpdResponse");

        String type = "RESPONSE";
        String mpdFileName = mpdFile.getName();
        String typeOfFile = typeOfFile(mpdFileName);

        byte[] bytesArray = readByteFromFile(mpdFile);
        long numberOfBytes = bytesArray.length;

        Log.d("connectionLogger", "Il numero di byte del file è: " + numberOfBytes);

        responseData = new ResponseObject(type, typeOfFile, mpdFileName, mpdFile.toString(), bytesArray, 0, 0);

        return responseData;

    }

    public ResponseObject setResponseFromTempCache(ServerCacheObject serverCacheObject){
        String type = "RESPONSE";
        String key = serverCacheObject.getKeyOfFile();
        String typeOfFile = "m4s";
        String fileName = serverCacheObject.getFileName();

        byte[] bytesArray = serverCacheObject.getFileBytes();
        long numberOfBytes = bytesArray.length;

        Log.d("connectionLogger", "Il numero  di byte del file da cache temp è: " +numberOfBytes);

        responseData = new ResponseObject(type, typeOfFile, fileName, key, bytesArray, 0, 0);

        return responseData;
    }

    public void releaseClientSocket() {
        try {
            clientSocket.close();
            Log.d("connectionLogger", "ClientSocket chiuso regolarmente");
            //Qui il thread chiude il socket e la connessione si interrompe forse qui può essere messo lo stream
            if(evaluationStreamFile != null) {
                Log.d("connectionLogger", "E' presente un EvaluationStreamFile che deve essere scritto");
                evaluationStream.generateEvaluationText(context, evaluationStreamFile);
            }
        }
        catch (IOException e){
            e.printStackTrace();
            Log.d("connectionLogger", "Errore nella chiusura del clientSocket");
        }
    }

    public File serializeCacheObject(ServerCacheObject serverCacheObject){
        String nomeVideo = serverCacheObject.getVideoPath();
        String keyVideo = serverCacheObject.getKeyOfFile();
        String[] keyVideoSplit = keyVideo.split("/");
        String nameServerCacheObject = keyVideoSplit[8] + "-" + keyVideoSplit[9];
        File cacheFile = new File(tempCacheDirectory + "//" + nomeVideo + "//");

        if(tempCacheDirectory.exists()){
            Log.d("connectionLogger", "Esiste");
        }
        else{
            Log.d("connectionLogger", "Non esiste");
        }

        if(cacheFile.exists()){
            Log.d("connectionLogger", "Esiste");
        }
        else{
            Log.d("connectionLogger", "Non esiste e la creiamo");
            cacheFile.mkdirs();
        }

        File serializeFile = new File(cacheFile, nameServerCacheObject);

        try {
            boolean fileExists = serializeFile.createNewFile();

            if(fileExists){
                Log.d("connectionLogger", "Il file esiste");
            }
            else{
                Log.d("connectionLogger", "Il file non esiste");
            }

            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(serializeFile));
            oos.writeObject(serverCacheObject);
            Log.d("connectionLogger", "File serializzato correttamente");
        }
        catch (Exception e){
            Log.d("connectionLogger", "Errore nella serializzazione del file");
        }
        return cacheFile;
    }

    public void downloadMissingChunk(String fileString){

        try {
            originalKey = fileString;

            String nomeChunkEstratto = estraiNomeMpd(fileString);
            File destinationFile = new File(tempDirectory + "//" +nomeChunkEstratto);

            URL website = new URL(fileString);

            //Codice per l'effettivo download del chunk
            BufferedInputStream bis = new BufferedInputStream(website.openStream());
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destinationFile));


            byte[] buffer = new byte[1024 * 1024];
            int read = 0;
            while ((read = bis.read(buffer)) != -1) {
                bos.write(buffer, 0, read);
            }
            bos.close();
            bis.close();

            byte[] fileBytes = readByteFromFile(destinationFile);
            ServerCacheObject serverCacheObject = new ServerCacheObject(destinationFile.toString(), originalKey, videoPath, nomeChunkEstratto, fileBytes);
            serializeCacheObject(serverCacheObject); //Problema
            deleteFile(destinationFile);

            ResponseObject responseObject = setResponseFromTempCache(serverCacheObject);
            sendBackResponse(responseObject);

        }

        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void deleteFile(File file){
        try{

            if(file.delete()){
                Log.d("connectionLogger", "File temporaneo eliminato con successo");
            }else{
                Log.d("connectionLogger", "File temporaneo non eliminato correttamente");
            }

        }catch(Exception e){

            Log.d("connectionLogger", "Eccezione durante l'eliminazione del file");
            e.printStackTrace();

        }
    }

    public String typeOfFile(String fileName){
        String returnString = fileName.substring(fileName.length() - 3);
        return returnString;
    }

    //E' per la cache di Exoplayer
    public void searchCacheForChunk(String cacheSpanKey){
        //Cerca in cache il cachespan relativo al segmento e lo invia al client
        Set<String> keysList = simpleCache.getKeys();
        for (String key : keysList) {
            if (cacheSpanKey.equals(key)) {
                //CacheSpan trovato
                try {
                    Log.d("connectionLogger", "CacheSpan trovato");
                    NavigableSet<CacheSpan> cacheSpanList = simpleCache.getCachedSpans(key);
                    CacheSpan cacheSpanToSend = cacheSpanList.pollFirst();
                    cacheSpanKeyToSend = key;
                    ResponseObject responseData = setResponse(cacheSpanToSend);
                    sendBackResponse(responseData);
                    //Qui inseriamo il pezzo di codice che serve ad incrementare la barra
                    //incrementCacheHit();
                    evaluationFragment.updateProgressDevice(positionOfDevice);
                    setEvaluationStreamFile(cacheHitObject.getPercentage(), 1);
                    return;
                } catch (NullPointerException e) {
                    Log.d("connectionLogger", "Non ci sono span per quella data chiave nel server");
                }
            }
        }

        Log.d("connectionLogger", "Nessun chunk trovato nella cache di Exoplayer");
        searchTempCacheForChunk(cacheSpanKey);
        //downloadMissingChunk(cacheSpanKey);
    }

    //E' per la cache temporanea
    public void searchTempCacheForChunk(String cacheSpanKey){

        //Cerca in cache il cachespan relativo al segmento e lo invia al client
        String[] keySplit = cacheSpanKey.split("/");
        String videoPath = keySplit[6] + "//" + keySplit[7];
        String keyToCheck = keySplit[8] + "-" + keySplit[9];
        File cacheFolder = new File(tempCacheDirectory + "//" + videoPath);
        File[] listOfFile = cacheFolder.listFiles();
        ServerCacheObject serverCacheObject;

        try {
            for (File fileEntry : listOfFile) {
                String[] fileSplit = fileEntry.toString().split("/");
                String fileToCheck = fileSplit[6];
                if (keyToCheck.equals(fileToCheck)) {
                    //CacheSpan trovato nella cache temporanea
                    serverCacheObject = deSerializeServerCacheObject(fileEntry);
                    if (serverCacheObject != null) {
                        Log.d("connectionLogger", "CacheSpan trovato in tempCache e deserializzato correttamente");
                        ResponseObject responseObject = setResponseFromTempCache(serverCacheObject);
                        sendBackResponse(responseObject);
                        evaluationFragment.updateProgressDevice(positionOfDevice);
                        setEvaluationStreamFile(cacheHitObject.getPercentage(), 1); //CacheHitObject è null
                        return;
                    }
                }
            }
        }
        catch (NullPointerException e){
            Log.d("connectionLogger", "La lista file della cache temp è vuota o null");
        }

        downloadMissingChunk(cacheSpanKey);
    }


    public void setEvaluationStreamFile(double percentage, int cacheHitCounter){
        evaluationStreamFile.setPercentage(percentage);
        evaluationStreamFile.setCacheHitCounter(cacheHitCounter);
    }

    public void closeEvaluationStreamFile(){
        evaluationStreamFile.getEndTimestamp();
        //Invia l'istanza allo stream che la salva come documento di testo
    }

    public ServerCacheObject deSerializeServerCacheObject(File file){
        ServerCacheObject serverCacheObject = null;

        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            serverCacheObject = (ServerCacheObject) ois.readObject();
        }
        catch (Exception e){
            Log.d("connectionLogger", "Errore nella deserializzazione del file");
            e.printStackTrace();
        }
        return serverCacheObject;
    }

    public void sendBackResponse(ResponseObject responseObject){
        //Mando il pacchetto indietro all'interno della response
        try {
            os = clientSocket.getOutputStream();
            oos = new ObjectOutputStream(new BufferedOutputStream(os));
            try {
                oos.writeObject(responseObject);
                oos.flush();
            } catch (SocketException e) {
                    oos.flush();
                    Log.d("connectionLogger", "Errore o disconnessione server mentre inviava un segmento");
            } catch (IOException e3) {
                Log.d("connectionLogger", "Errore e3");
            } catch (NullPointerException e) {
                Log.d("connectionLogger", "Errore in oos");
                e.printStackTrace();
            }
            //Poi qua bisogna vedere come fare perchè in questo modo viene creato un thread ogni richiesta pacchetto
            //E non va bene, bisogna creare un thread ogni richiesta connessione
        }
        catch (Exception e){
            Log.d("connectionLogger", "Eccezione nello spedire indietro la richiesta");
            e.printStackTrace();
        }
    }

    public void checkRequest(JSONObject jsonRequest){
        try {
            Log.d("connectionLogger", "Siamo in searchCache");
            videoPath = (String) jsonRequest.get("videoPath");
            cacheSpanKey = (String) jsonRequest.get("cacheSpanKey");

            String[] videoPathSplitter = videoPath.split("/");
            String videoPathForMilliseconds = videoPathSplitter[0];

            VideoLengthChecker videoLengthChecker = new VideoLengthChecker();
            int videoMilliseconds = videoLengthChecker.getMillisecondsVideo(videoPathForMilliseconds);
            evaluationFragment.setMaxProgressDevice(positionOfDevice, videoMilliseconds);
            evaluationStreamFile.setNomeVideo(videoPathForMilliseconds);

            Log.d("connectionLogger", "Cerchiamo nella cache un segmento con videoPath: " + videoPath + " e chunkKey: " + cacheSpanKey);
            cacheDir = new File(context.getCacheDir(), videoPath);
            simpleCache = new SimpleCache(cacheDir, evictor);

            String typeOfFile = typeOfFile(cacheSpanKey);

            if(typeOfFile.equals("mpd")) {
                //Richiedo un mpd quindi lo scarico e lo invio semplicemente
                searchCacheForMpd(cacheSpanKey);
            }
            else{
                searchCacheForChunk(cacheSpanKey);
            }

        }
        catch (JSONException e){
            Log.d("threadServerLogger", "Errore nel Json request arrivato");
            e.printStackTrace();
        }
    }

    public String estraiNomeMpd(String videoMpdDaEstrarre){

        //Estraiamo il nome semplice senza path del mpd da salvare nella cartella mpds
        String[] videoMpdSplit = videoMpdDaEstrarre.split("/");
        String nomeMpd = videoMpdSplit[videoMpdSplit.length - 1];
        return nomeMpd;
    }

    public void downloadMissingMpd(String videoMpd){
        try {
            Log.d("connectionLogger", "Siamo in downloadMissingMpd");
            Log.d("connectionLogger", "Inizio download con videoMpd " + videoMpd);

            String nomeMpdEstratto = estraiNomeMpd(videoMpd);
            File destinazioneFileMpd = new File(mpdDirectory.getPath() + "//" + nomeMpdEstratto);

            if (!destinazioneFileMpd.exists()) {
                destinazioneFileMpd.createNewFile();
            }

            URL website = new URL(videoMpd);

            //Codice per l'effettivo download dell'mpd
            BufferedInputStream bis = new BufferedInputStream(website.openStream());
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destinazioneFileMpd));

            byte[] buffer = new byte[1024 * 1024];
            int read = 0;
            while ((read = bis.read(buffer)) != -1) {
                bos.write(buffer, 0, read);
            }
            bos.close();
            bis.close();

            searchCacheForMpd(videoMpd);
        }
        catch (Exception e){
            Log.d("playerLogger", "Connessione non presente, mpd non scaricato");
            e.printStackTrace();
        }
    }

    public void searchCacheForMpd(String nomeMpdFile) {
        String nomeMpdEstratto = estraiNomeMpd(nomeMpdFile);
        try {
            Log.d("connectionLogger", "Siamo nel searchCacheForMpd");
            for (final File fileEntry : mpdDirectory.listFiles()) {
                if (fileEntry.getName().equals(nomeMpdEstratto)) {
                    Log.d("connectionLogger", "Abbiamo trovato l'mpd corrispondente nella cache server");
                    //Mpd trovato in cache credo la request e lo invio indietro al client
                    ResponseObject responseData = setMpdResponse(fileEntry);
                    sendBackResponse(responseData);
                    return;
                }
            }
        } catch (NullPointerException e) {
            Log.d("connectionLogger", "La cartella mpds della cache Server è vuota");
            downloadMissingMpd(nomeMpdFile);
        }

        //Non trovo l'mpd quindi lo scarico
        Log.d("connectionLogger", "Mpd non trovato quindi avviamo il prossimo metodo");
        downloadMissingMpd(nomeMpdFile);
    }

    @Override
    public void run() {
        d("connectionLogger", "Server Thread Avviato");
        //Dichiarazione dei socket e di tutti gli input/output stream

        try {
            try {
                Log.d("connectionLogger", "Siamo sopra l'inizializzazione dei socket");
                inputstream = clientSocket.getInputStream();
                ois = new ObjectInputStream(new BufferedInputStream(inputstream));
            } catch (SocketException e) {
                clientSocket.close();
                stop = true;
                releaseClientSocket();
            }

            //Prima di entrare nel while, il server deve ottenere le informazioni del dispositivo client che si è connesso e inviarle al fragment
            try {
                String connectedDeviceName = (String) ois.readObject();
                //ProgressBar progressBar = new ProgressBar(context);
                cacheHitObject = new CacheHitObject(connectedDeviceName, 0);
                addCacheHitToFragment(cacheHitObject);
                //Qua aggiungo la creazione di un'istanza dell'EvaluationStreamFile
                evaluationStreamFile = new EvaluationStreamFile(connectedDeviceName);
                //Dopo aver ottenuto il nome del dispositivo, dobbiamo inviarlo al fragment per aggiungerlo all'oggetto con la progress bar
            }
            catch (ClassNotFoundException e){
                e.printStackTrace();
            }

            //Forse qui si necessita di un nuovo inputstream e ois per la sincronizzazione tra i dispositivi

            while (!stop) {
                d("connectionLogger", "Siamo nel while del server thread");
                //Il thread va in loop fino a che non si decide di fermarlo
                try {
                    //Permette la lettura del database della cache
                    // Gets the data repository in write mode
                    /**
                     * Create a server socket and wait for client connections. This
                     * call blocks until a connection is accepted from a client
                     */
                    Log.d("connectionLogger", "Siamo sopra l'inputStream");

                    try {
                        JSONObject jsonRequest = null;
                        try {
                            String jsonRequestString = (String) ois.readObject();
                            jsonRequest = new JSONObject(jsonRequestString);
                            Log.d("connectionLogger", "ottenuto lato client questa request: " + jsonRequest.toString());
                        } catch (SocketException e) {
                            stop = true;
                            Log.d("connectionLogger", "Errore disconnessione avvenuta durante la ricezione delle richiesta");
                            e.printStackTrace();
                        } catch (ClassNotFoundException e) {
                            Log.d("connectionLogger", "ClassNotFoundException");
                            e.printStackTrace();
                            stop = true;
                        }

                        if (jsonRequest != null) {
                            //Codice per leggere la richiesta e cercare nel database se è presente un pacchetto
                            String jsonRequestType = (String) jsonRequest.get("type");
                            if (jsonRequestType.equals("REQUEST")) {
                                //Uso i metodi della SimpleCache per ottenere la lista delle keys del server
                                checkRequest(jsonRequest);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("connectionLogger", "Errore con la ricezione lato server del JSON");
                        //releaseClientSocket();
                        stop = true;
                    }

                    //////////DA QUA IN POI è DA SISTEMARE PERCHè C'è IL RITORNO DEL PACCHETTO

                    //Qui va la condizione di chiusura con controllo di disconnessione del client

                } catch (IOException e) {
                    d("connectionLogger", "Errore nel Server Thread con messaggio: " + e.getMessage());
                    e.printStackTrace();
                    //releaseClientSocket();
                    stop = true;
                }

            }
        }
        catch (IOException e){
            Log.d("connectionLogger", "Errore nell'inizializzazione dei socket");
        }
        finally {
            try {
                ois.close();
                oos.close();
                inputstream.close();
                releaseClientSocket();
                System.out.println("...Stopped");
            } catch(IOException ioe) {
                ioe.printStackTrace();
            }
            catch (NullPointerException e){
                e.printStackTrace();
            }
        }
    }

}


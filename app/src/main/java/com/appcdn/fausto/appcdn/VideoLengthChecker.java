package com.appcdn.fausto.appcdn;

/**
 * Created by Utente on 01/12/2017.
 */

public class VideoLengthChecker {

    //BigBuckBunny
    private String bigBuckBunny = "BigBuckBunny";
    private double secondsBigBuckBunny = 596.46;
    private int millisecondsBigBuckBunny = 596460;

    //Elephant's Dream
    private String elephantsDream = "ElephantsDream";
    private double secondsElephantsDream = 653.79;
    private int millisecondsElephantsDream = 653790;

    //Of Forest And Men
    private String forestMen = "OfForestAndMen";
    private double secondsForestMen = 453.2;
    private int millisecondsForestMen = 453200;

    //RedBull Play Streets
    private String redBullPlayStreets = "RedBullPlayStreets";
    private double secondsRedBull = 5242.94;
    private int millisecondsRedBull = 5242940;

    //Tears Of Steel
    private String tearsOfSteel = "TearsOfSteel";
    private double secondsTearsOfSteel = 734.17;
    private int millisecondsTearsOfSteel = 734170;

    //The Swiss Account
    private String theSwissAccount = "TheSwissAccount";
    private double secondsTheSwissAccount = 3453.98;
    private int millisecondsTheSwissAccount = 3453980;

    //Valkaama
    private String valkaama = "Valkaama";
    private double secondsValkaama = 4652.61;
    private int millisecondsValkaama = 4652610;

    public VideoLengthChecker() {
        //Default constructor
    }

    public double getSecondsVideo(String videoChoice){
        double response = 0;
        if(videoChoice.equals(bigBuckBunny)){
            response = secondsBigBuckBunny;
        }
        else if(videoChoice.equals(elephantsDream)){
            response = secondsElephantsDream;
        }
        else if(videoChoice.equals(forestMen)){
            response = secondsForestMen;
        }
        else if(videoChoice.equals(redBullPlayStreets)){
            response = secondsRedBull;
        }
        else if(videoChoice.equals(tearsOfSteel)){
            response = secondsTearsOfSteel;
        }
        else if(videoChoice.equals(theSwissAccount)){
            response = secondsTheSwissAccount;
        }
        else if(videoChoice.equals(valkaama)){
            response = secondsValkaama;
        }
        else{
            response = 0;
        }
        return response;
    }

    public int getMillisecondsVideo(String videoChoice){
        int response = 0;
        if(videoChoice.equals(bigBuckBunny)){
            response = millisecondsBigBuckBunny;
        }
        else if(videoChoice.equals(elephantsDream)){
            response = millisecondsElephantsDream;
        }
        else if(videoChoice.equals(forestMen)){
            response = millisecondsForestMen;
        }
        else if(videoChoice.equals(redBullPlayStreets)){
            response = millisecondsRedBull;
        }
        else if(videoChoice.equals(tearsOfSteel)){
            response = millisecondsTearsOfSteel;
        }
        else if(videoChoice.equals(theSwissAccount)){
            response = millisecondsTheSwissAccount;
        }
        else if(videoChoice.equals(valkaama)){
            response = millisecondsValkaama;
        }
        else{
            response = 0;
        }
        return response;
    }

}

package com.appcdn.fausto.appcdn;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Player.RepeatMode;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.AdaptiveMediaSourceEventListener;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector.Parameters;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;

import java.io.File;
import java.io.IOException;


public class VideoPlayerActivity extends AppCompatActivity {

    SimpleExoPlayer player = null;

    private Intent intent;
    private String uriVideoString;
    private String videoPath;

    private Uri uri = null;
    PlaybackStateCompat.Builder mStateBuilder;
    CacheDataSourceFactoryHandler cacheDataSourceFactoryHandler;
    DataSource.Factory dataSourceFactory;
    DefaultTrackSelector trackSelector;
    int resumeWindow;
    long resumePosition;
    DashMediaSource mediaSource;
    NotifyClass notifyClass;
    ThreadNotify threadNotify;

    boolean clientActivated;

    boolean isExecuted = false;
    String chunkExecuted = "temp";

    File mpdDirectory;
    CreateDirectory createDirectory;

    MainActivity.SharedMpdDirectory sharedMpdDirectory;

    //long realDurationMillis;
    //boolean durationSet;

    Handler eventHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                //Gestore messaggio
            }
        }
    };

    public Uri stringToUri(String urlString){
        uri = Uri.parse(urlString);
        return uri;
    }

    public void getFullscreen(){
        //Fullscreen
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    //Cache source per connessione internet
    private DashMediaSource buildCacheSource(Uri uri) {
        DashMediaSource dashMediaSource = new DashMediaSource(uri, dataSourceFactory,
                new DefaultDashChunkSource.Factory(dataSourceFactory), eventHandler, adaptiveMediaSourceEventListener);
        return dashMediaSource;
    }

    private void startPlayerFromPosition(){
        try {
            long positionInMillisecond = getTrackPositionInMillisecond();
            player.seekTo(positionInMillisecond);
            player.setPlayWhenReady(true);
            player.prepare(mediaSource);
        }
        catch (NullPointerException e){
            Log.d("playerLogger", "Null reference nel player in onLoadCanceled");
        }
    }

    public String estraiNomeMpd(String videoMpdDaEstrarre){
        //Estraiamo il nome semplice senza path del mpd da salvare nella cartella mpds
        String[] videoMpdSplit = videoMpdDaEstrarre.split("/");
        String nomeMpd = videoMpdSplit[videoMpdSplit.length - 1];
        return nomeMpd;
    }

    private void setLocalMpd(String mpdVideoString, String mpdDirectoryString){
        Log.d("notifyThread", "Siamo nel setLocalMpd");
        //Quando l'mpd arriva e viene correttamente salvato in cache viene sostituito l'uri online con l'uri locale file:///
        //Per fare questo viene preso l'mpd e viene sostituito http:// con file:/// più il path per riferirsi all'mpd locale
        String mpdEstratto = estraiNomeMpd(mpdVideoString);
        //Aggiungiamo file:/// e il path e poi usiamo replace
        String uriMpdComplete = "file:///" + mpdDirectoryString + "//" + mpdEstratto;
        //Convertiamo tutto in uri
        Uri newMpdUri = stringToUri(uriMpdComplete);
        //Sostituiamo il vecchio mpd con il nuovo
        mediaSource.replaceManifestUri(newMpdUri);
    }

    public void threadUseSetLocalMpd(){
        setLocalMpd(uriVideoString, mpdDirectory.toString());
    }
    public void inizializzazionePlayer(String videoPath){

        ///////////Exoplayer

        if(player == null) {
            // 1. Create a default TrackSelector
            Handler mainHandler = new Handler();
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

            /* -------------------------- */
            //Definisco che il bitrate utilizzato deve sempre essere il minimo
            DefaultTrackSelector.Parameters parameters = new Parameters();
            parameters.withMaxVideoBitrate(45514);
            trackSelector.setParameters(parameters);
            /* -------------------------- */

            // 2. Create a default LoadControl
            LoadControl loadControl = new DefaultLoadControl();

            // 3. Create the player
            player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(this), trackSelector, loadControl);

            // Bind the player to the view.
            SimpleExoPlayerView simpleExoPlayerView = (SimpleExoPlayerView) findViewById(R.id.video_player);
            simpleExoPlayerView.setPlayer(player);

            //Aggiunto un listener per monitorare il player
            player.addListener(eventListener);
        }

        if (resumeWindow != C.INDEX_UNSET) {
            player.seekTo(resumeWindow, resumePosition);
        }

        //Inizializzazione MediaSource per singolo video, da aggiungere un if or else per l'intent che viene passato dalla main activity
        //uri = stringToUri(uriVideoString);

        //Inizializzazione cache path
        cacheDataSourceFactoryHandler.changeVideoPath(videoPath);

        //mediaSource = buildCacheSource(uri);
        inizializzazioneDashPlayer(uriVideoString);
        player.prepare(mediaSource, resumeWindow != C.INDEX_UNSET, false);
        player.setPlayWhenReady(true);
    }

    public void startPlayer(){
        player.setPlayWhenReady(true);
        player.getPlaybackState();
    }

    public void pausePlayer(){
        getTrackPosition();
        player.setPlayWhenReady(false);
        player.getPlaybackState();
    }

    public void eliminaPlayer(){
        if(player != null) {
            getTrackPosition();
            player.release();
            player = null;
            trackSelector = null;
        }
    }

    public void getTrackPosition(){
        resumeWindow = player.getCurrentWindowIndex();
        resumePosition = player.isCurrentWindowSeekable() ? Math.max(0, player.getCurrentPosition())
                : C.TIME_UNSET;
    }

    private void clearTrackPosition() {
        resumeWindow = C.INDEX_UNSET;
        resumePosition = C.TIME_UNSET;
    }

    public long getTrackPositionInMillisecond(){
        long positionInMillisecond = player.getCurrentPosition();
        return positionInMillisecond;
    }

    //Questo metodo viene utilizzato per richiedere al servizio di fare una richiesta per un pacchetto mancante se ci si trova in modalità client
    private void callService(String chunkKey, String videoPath){
        //Come argomento bisogna passare le informazioni da inviare al service in modo tale che il service effettui la richiesta
        Intent intent = new Intent(this, DeviceConnectionService.class);
        intent.putExtra("chunkKey", chunkKey);
        intent.putExtra("videoPath", videoPath);
        intent.putExtra("sourceIntent", "VideoPlayerActivity.class");
        //intent.putExtra("durataVideo", realDurationMillis); //Questo valore serve per comunicare al server la durata intera del video che si intende visualizzare
        startService(intent);
    }

    public boolean checkIfExecuted(Uri uri){
        String chunkName = uri.toString();
        if(!chunkName.equals(chunkExecuted)){
            chunkExecuted = chunkName;
            isExecuted = false;
        }
        else if(chunkName.equals(chunkExecuted)){
            isExecuted = true;
        }
        return isExecuted;
    }

    //Metodo che rimpiazza un mpd con un altro e nel nostro caso l'mpd online con il path locale
    public void replaceMpdWithLocal(Uri newMpd){
        //Possiamo rimpiazzare l'mpd online con il locale con il replaceMpd della classe DashMediaSource

    }

    public Uri checkMpdInCache(String videoPath){

        createDirectory.directoryCreator(mpdDirectory);

        try {
            for (File fileEntry : mpdDirectory.listFiles()) {
                String fileName = fileEntry.getName();
                if (fileName.equals(videoPath)) {
                    //Trovato il file nella cache e quindi inizializziamo il player con questo file
                    Uri uriFile = Uri.fromFile(fileEntry);
                    return uriFile;
                }
            }
        }
        catch (NullPointerException e){
            Log.d("playerLogger", "mpdDirectory vuota");
            return null;
        }

        //File non trovato e quindi lo scarica lui da internet oppure va in onLoadError e lo richiede ad un altro dispositivo
        return null;

    }

    public void inizializzazioneDashPlayer(String mpdKey){
        Uri mpdUriFile = checkMpdInCache(mpdKey);
        if(mpdUriFile != null){
            mediaSource = buildCacheSource(mpdUriFile);
        }
        else{
            Uri classicMpdUriFile = stringToUri(mpdKey);
            mediaSource = buildCacheSource(classicMpdUriFile);
        }
    }

    private void startThreadNotify(){
        threadNotify = new ThreadNotify(this, notifyClass, this);
        threadNotify.start();
    }

    public void mpdDirectoryCreator(File mpdDirectoryFile){
        createDirectory = new CreateDirectory();
        createDirectory.directoryCreator(mpdDirectoryFile);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        getFullscreen();

        cacheDataSourceFactoryHandler = new CacheDataSourceFactoryHandler(this);

        //Creazione se non presente della cartella dove sistemiamo gli mpd locali
        sharedMpdDirectory = new MainActivity.SharedMpdDirectory();
        mpdDirectory = sharedMpdDirectory.getMpdDirectory();
        mpdDirectoryCreator(mpdDirectory);

        intent = getIntent();
        uriVideoString = intent.getStringExtra("uriVideo");
        videoPath = intent.getStringExtra("videoPath");
        clientActivated = intent.getBooleanExtra("clientActivated", false);

        dataSourceFactory = cacheDataSourceFactoryHandler.buildDataSourceFactory(true);

        ///////////Exoplayer

        inizializzazionePlayer(videoPath);
    }

    protected void onStart(){
        super.onStart();
        if(player == null) {
            inizializzazionePlayer(videoPath);
        }
        else{
            startPlayer();
        }
    }

    protected void onResume(){
        super.onResume();
        getFullscreen();
        startThreadNotify();
        if(player == null) {
            inizializzazionePlayer(videoPath);
        }
        else{
            startPlayer();
        }
    }
    protected void onPause(){
        super.onPause();
        pausePlayer();
    }

    protected void onStop(){
        super.onStop();
        pausePlayer();
        threadNotify.stopNotifyThread();
    }

    protected void onDestroy(){
        super.onDestroy();
        eliminaPlayer();
        clearTrackPosition();
        threadNotify.stopNotifyThread();
    }

    public void onNewIntent(Intent intent) {
        eliminaPlayer();
        clearTrackPosition();
        setIntent(intent);
    }

    Player.EventListener eventListener = new Player.EventListener() {
        /*
        Metodo Listner che si attiva quando la timeline o il manifest vengono ricaricati
        Può succedere che se la timeline cambia viene rilevato un position discontinuity e non viene riportato in onPositionDiscontinuity()
    */
        @Override
        public void onTimelineChanged(Timeline timeline, Object manifest) {
            try {
                Log.d("playerLogger", "onTimelineChanged() triggered con timeline: " + timeline.toString() + " e manifest: " + manifest.toString());
            }
            catch (NullPointerException e){
                Log.d("playerLogger", "onTimelineChanged() triggered con timeline o manifest null");
            }

        }

        //Viene richiamato quando la track viene cambiata
        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
            Log.d("playerLogger", "onTracksChanged() triggered con trackGroups: " + trackGroups.toString() + " e trackSelectionArray: " + trackSelections.toString());
        }

        //Viene richiamato quando il player avvia o stoppa la riproduzione
        @Override
        public void onLoadingChanged(boolean isLoading) {
            Log.d("playerLogger", "onLoadingChanged() triggered con isLoading: " + isLoading);
        }

        //Viene richiamato quando i valori ritornati da getPlayWhenReady e getPlaybackState cambiano
        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            Log.d("playerLogger", "onPlayerStateChanged() triggered con playWhenReady: " + playWhenReady + " e playbackState: " + playbackState);
            /*
            if (playbackState == Player.STATE_READY && !durationSet) {
                realDurationMillis = player.getDuration();
                durationSet = true;
            }
            */
        }

        @Override
        public void onRepeatModeChanged(@RepeatMode int repeatMode) {
            Log.d("playerLogger", "onRepeatModeChanged() triggered con repeatMode: " + repeatMode);
        }

        /*
            Viene richiamato quando il player va in errore
            Quando viene richiamato il player va in STATE_IDLE immediatamente
        */
        @Override
        public void onPlayerError(ExoPlaybackException error) {
            Log.d("playerLogger", "onPlayerError triggered con error: " + error.toString());
        }

        //Viene richiamato quando c'è una discontinuità nella riproduzione senza alterare la timeline.
        @Override
        public void onPositionDiscontinuity() {
            Log.d("playerLogger", "onPositionDiscontinuity() triggered");
            if (resumeWindow != C.INDEX_UNSET) {
                getTrackPosition();
            }
        }

        @Override
        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters){
            Log.d("playerLogger", "onPlaybackParametersChanged() triggered con: " + playbackParameters.toString());
        }
    };

    AdaptiveMediaSourceEventListener adaptiveMediaSourceEventListener = new AdaptiveMediaSourceEventListener() {
        @Override
        public void onLoadStarted(DataSpec dataSpec, int dataType, int trackType, Format trackFormat, int trackSelectionReason, Object trackSelectionData, long mediaStartTimeMs, long mediaEndTimeMs, long elapsedRealtimeMs) {
            Log.d("playerLogger", "onLoadStarted() triggered sul segmento con key: " + dataSpec.toString());
        }

        @Override
        public void onLoadCompleted(DataSpec dataSpec, int dataType, int trackType, Format trackFormat, int trackSelectionReason, Object trackSelectionData, long mediaStartTimeMs, long mediaEndTimeMs, long elapsedRealtimeMs, long loadDurationMs, long bytesLoaded) {
            Log.d("playerLogger", "onLoadCompleted() triggered sul segmento con key: " + dataSpec.toString());
        }

        @Override
        public void onLoadCanceled(DataSpec dataSpec, int dataType, int trackType, Format trackFormat, int trackSelectionReason, Object trackSelectionData, long mediaStartTimeMs, long mediaEndTimeMs, long elapsedRealtimeMs, long loadDurationMs, long bytesLoaded) {
            Log.d("playerLogger", "onLoadCanceled() triggered sul segmento con key: " + dataSpec.uri);
            startPlayerFromPosition();
        }

        @Override
        public void onLoadError(DataSpec dataSpec, int dataType, int trackType, Format trackFormat, int trackSelectionReason, Object trackSelectionData, long mediaStartTimeMs, long mediaEndTimeMs, long elapsedRealtimeMs, long loadDurationMs, long bytesLoaded, IOException error, boolean wasCanceled) {
            Log.d("playerLogger", "onLoadError() triggered 1 sul segmento con dataSpec: " + dataSpec.toString() + " e dataType: " + dataType + " e trackType: " + trackType);
            try {
                Log.d("playerLogger", "onLoadError() triggered 2 sul segmento con trackFormat: " + trackFormat.toString());
            }
            catch (NullPointerException e){
                Log.d("playerLogger", "onLoadError() trackFormat null");
            }
            try{
                Log.d("playerLogger", "onLoadError() triggered 3 sul segmento con trackSelectionData: " + trackSelectionData.toString());
            }
            catch (NullPointerException e){
                Log.d("playerLogger", "onLoadError() trackSelectionData null");
            }
            //Probabile metodo del listener che ritorna la key del pacchetto mancante

            if(clientActivated == true) {
                //Log.d("playerLogger", "Siamo entrati nel clientActivated dentro onLoadError()");
                isExecuted = checkIfExecuted(dataSpec.uri);
                if(isExecuted == false) {
                    Log.d("playerLogger", "Siamo nell'isExecuted dentro onLoadError()");
                    callService(dataSpec.uri.toString(), videoPath);
                }
                else{
                    Log.d("playerLogger", "isExecuted è true quindi non dovrebbe richiamare il metodo");
                }
            }
            else{
                Log.d("playerLogger", "Errore nel reperire un segmento in modalità normale. Prego cambiare modalità in modalità client");
            }
        }

        @Override
        public void onUpstreamDiscarded(int trackType, long mediaStartTimeMs, long mediaEndTimeMs) {
            Log.d("playerLogger", "onUpstreamDiscarded() triggered con trackType: " + trackType + " mediaStartTimeMs: " + mediaStartTimeMs + " mediaEndTimeMs: " + mediaEndTimeMs);
            //Metodo per cambiare formato del video in esecuzione a runtime
        }

        @Override
        public void onDownstreamFormatChanged(int trackType, Format trackFormat, int trackSelectionReason, Object trackSelectionData, long mediaTimeMs) {
            Log.d("playerLogger", "onDownstreamFormatChanged() triggered con trackType: " + trackType + " trackFormat: " + trackFormat + " trackSelectionReason: " + trackSelectionReason + " trackSelectionData: " + trackSelectionData + " mediaTimeMs: " + mediaTimeMs);
            //Metodo per cambiare formato del video in esecuzione a runtime
        }
    };

    public static class NotifyClass{
        private static boolean isArrivedMpd = false;
        private static boolean isArrivedChunk = false;

        public static void setMpdTrue(){
            NotifyClass.isArrivedMpd = true;
        }
        public static void setChunkTrue(){
            NotifyClass.isArrivedChunk = true;
        }

        public static void setMpdFalse(){
            NotifyClass.isArrivedMpd = false;
        }
        public static void setChunkFalse(){
            NotifyClass.isArrivedChunk = false;
        }

        public static boolean getIsArrivedMpd(){
            return NotifyClass.isArrivedMpd;
        }
        public static boolean getIsArrivedChunk(){
            return NotifyClass.isArrivedChunk;
        }
    }

}

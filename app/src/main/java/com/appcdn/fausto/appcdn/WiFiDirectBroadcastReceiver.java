package com.appcdn.fausto.appcdn;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;

/**
 * Created by Utente on 12/07/2017.
 */

public class WiFiDirectBroadcastReceiver extends BroadcastReceiver {

    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private Context context;

    private WifiP2pManager.PeerListListener peerListListener;

    private WifiP2pManager.ConnectionInfoListener connectionListener;
    private WifiP2pManager.ConnectionInfoListener disconnectionListener;

    public WiFiDirectBroadcastReceiver(WifiP2pManager mManager, WifiP2pManager.Channel mChannel, Context context, WifiP2pManager.PeerListListener peerListListener) {
        super();
        this.mManager = mManager;
        this.mChannel = mChannel;
        this.context = context;

        this.peerListListener = peerListListener;
    }

    public void setConnectionListener(WifiP2pManager.ConnectionInfoListener connectionListener){
        this.connectionListener = connectionListener;
    }

    public void setDisconnectionListener(WifiP2pManager.ConnectionInfoListener disconnectionListener){
        this.disconnectionListener = disconnectionListener;
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        String action = intent.getAction();

        //Questo if rappresenta cosa succede se il p2p è abilitato o disabilitato
        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {

            Log.d("debugTime","Siamo nello State Changed Action");
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                // Wifi P2P is enabled

                Log.d("debugTime","Wifi P2P Enabled");

            } else {
                // Wi-Fi P2P is not enabled
                Log.d("debugTime","Wifi P2P not Enabled");
            }
        }

        //Questo rappresenta cosa succede se la lista dei peer cambia
        else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {

            Log.d("debugTime","Siamo entrati nel BoradcastReceiver in peers changed action");

            // The peer list has changed!  We should probably do something about
            // that.

            // request available peers from the wifi p2p manager. This is an
            // asynchronous call and the calling activity is notified with a
            // callback on PeerListListener.onPeersAvailable()

            if (mManager != null) {
                Log.d("debugTime", "Siamo sopra il requestPeers");
                mManager.requestPeers(mChannel, peerListListener);
            }

            //Questo rappresenta cosa succede se la connettività p2p cambia
        }

            else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {

                Log.d("debugTime", "Siamo in Connection Changed Action");

                // Connection state changed!  We should probably do something about
                // that.

                NetworkInfo networkInfo = (NetworkInfo) intent
                    .getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);

                if (networkInfo.isConnected()) {

                    // we are connected with the other device, request connection
                    // info to find group owner IP
                    if(connectionListener != null){
                        mManager.requestConnectionInfo(mChannel, connectionListener);
                    }


                }
                else {
                    // It's a disconnect
                    //Questo è il metodo che devo usare per spegnere i thread durante la disconnessione
                    if(DeviceConnectionService.ThreadActiveInstance.getInstanceThreadClient() != null){
                        //Il thread attivo è il client e quindi in disconnessione terminiamo il client
                        DeviceConnectionService.ThreadActiveInstance.getInstanceThreadClient().closeConnection();
                        MainActivity.SharedClientActivated.setClientActivated(false);
                    }
                    else if(DeviceConnectionService.ThreadActiveInstance.getInstanceThreadPoolServer() != null){
                        DeviceConnectionService.ThreadActiveInstance.getInstanceThreadPoolServer().stop();
                        MainActivity.SharedServerActivated.setServerActivated(false);
                    }
                    else{

                    }

                }

            }

            //Questo rappresenta cosa succede se la configurazione del dispositivo cambia
            else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {

                Log.d("debugTime","This Device Changed Action");

            }
        }
}
